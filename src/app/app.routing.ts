import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { GridComponent } from './ui-components/grid/grid.component';
import {SchduledCallComponent} from './pages/schduledcall/schduled_call/schduled_call.component';
const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'grid', component: GridComponent },
  {path: 'schduledcall', component: SchduledCallComponent},
  { path: 'style-guide', loadChildren: './ui-components/style-guide/styleguide.module#StyleguideModule' },
  { path: 'family-allowance', loadChildren: './pages/family-allowance/family-allowance.module#FamilyAllowanceModule' },
  { path: 'tmo-home', loadChildren: './pages/tmo-home/tmo.home.module#TmoModule' },
  { path: 'error-handling', loadChildren: './pages/error-handling/error-handling.module#ErrorHandlingModule' },
  { path: 'port-in', loadChildren: './pages/port-in/port-in.module#PortInModule' },
  { path: 'refill', loadChildren: './pages/refill/refill.module#RefillModule' }
];

export const routing = RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules});
