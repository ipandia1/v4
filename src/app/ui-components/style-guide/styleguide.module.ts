import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// routing module
import { StyleguideRoutingModule } from './styleguide-routing.module';
// reusable module
import { ReusableModule } from '../reusable/reusable.module';
// Style guide components: Alphabetical order

@NgModule({
  imports: [
    CommonModule,
    StyleguideRoutingModule,
    ReusableModule
  ],
  declarations: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StyleguideModule { }
