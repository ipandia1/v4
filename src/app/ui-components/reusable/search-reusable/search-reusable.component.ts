import { Component, OnInit, Input, ElementRef, Output, EventEmitter, HostListener, ViewChild, Renderer2 } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-search-reusable',
  templateUrl: './search-reusable.component.html',
  styleUrls: ['./search-reusable.component.scss']
})
export class SearchReusableComponent implements OnInit {
  constructor(private _ref: ElementRef, private renderer: Renderer2) {
    
    if (this.persistent === true) {
      this.showInput = true;
    }
  }

  
  @ViewChild('searchField') searchField: ElementRef;
  // tslint:disable-next-line:no-input-rename
  @Input('placeholder') dynamicplaceholder = 'Search';
  // tslint:disable-next-line:no-input-rename
  @Input('maxlength') maxlength = '45';
  // tslint:disable-next-line:no-input-rename
  @Input('inputArray') itemsInPage: Array<any> = [];
  // tslint:disable-next-line:no-input-rename
  @Input('animationType') animationType = 'right';
  // tslint:disable-next-line:no-input-rename
  @Input('crossIcon') crossIcon = 'clear-icon clear-icon-xs-sm';
  // tslint:disable-next-line:no-input-rename
  @Input('persistent') persistent;
  // tslint:disable-next-line:no-input-rename
  @Input('elementId') elementId;
  // tslint:disable-next-line:no-input-rename
  @Input('tabIndex') tabIndex;
  // tslint:disable-next-line:no-output-rename
  @Output('callBackValue') callBackValue = new EventEmitter<string>();

  inputtextfield: inputtextfield;
  copyOfItemsInPage: Array<any> = [];
  hoverItems: Array<any> = [];
  recentSearches: any[] = [];
  filteredList: any[] = [];
  showrecentSearches = false;
  showRecentSearchesElements = false;
  showSuggestionsElements = false;
  selectedItem = false;
  mouseclickevent = false;
  // tslint:disable-next-line:member-ordering
  temporaryInput = '';
   a;

  // tslint:disable-next-line:member-ordering
  showDiv1 = false;
  // tslint:disable-next-line:member-ordering
  unshift = false;
  // tslint:disable-next-line:member-ordering
  showInput = false;
  // tslint:disable-next-line:member-ordering
  showInput1 = false;
  // tslint:disable-next-line:member-ordering
  initialAnimation = false;
  placeholder = '';

  public divFocusOutFunction(event: any): void {
    let targetid = '';
    if (event.relatedTarget != null) {
      targetid = event.relatedTarget.id;
    }

    if (targetid !== 'test') {
      // console.log(event.target.id);
      // tslint:disable-next-line:curly
      if (event.target.children[1] === undefined)
        this.showInput = false;
      this.filteredList = [];
      this.showDiv1 = false;
      this.selectedItem = true;
      this.initialAnimation = true;
      this.callBackValue.emit(this.inputtextfield.inputText);
    }
  }

  @HostListener('keydown', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    const kcode = event.keyCode;
    if (kcode === 13) {
      const selectedelement = this._ref.nativeElement.querySelectorAll('.li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
      if (selectedelement.length !== 0) {
        selectedelement[0].classList.add('li-style-searchhover');
        selectedelement[0].classList.add('li-style-searchhover-active');
      }
      event.preventDefault();
    }
  }
  recentSearchesFunc(value) {
    if (value !== '' && this.recentSearches.indexOf(value) === -1) {
      this.recentSearches.unshift(value);
    }
    if ((this.unshift && this.recentSearches.length > 1) || (value !== '' && this.recentSearches.indexOf(value) !== -1)) {
      // tslint:disable-next-line:prefer-const
      let index = this.recentSearches.indexOf(value);
      if (index !== -1) {
        this.recentSearches.splice(index, 1);
      }
      this.recentSearches.unshift(value);
      this.unshift = false;
    }
  }

  filter(event: any) {
    let searchmenu = this._ref.nativeElement.querySelectorAll('.search-menu');
    if (event.keyCode === 40) {
      // tslint:disable-next-line:max-line-length
      if (this._ref.nativeElement.querySelectorAll('.show-recent-search').length > 0 || this._ref.nativeElement.querySelectorAll('.show-suggestions').length > 0) {
        let items = this._ref.nativeElement.querySelectorAll('.li-style-suggestions') as HTMLCollectionOf<HTMLElement>;
        if (items.length === 0) {
          items = this._ref.nativeElement.querySelectorAll('.li-style-search') as HTMLCollectionOf<HTMLElement>;
          searchmenu = this._ref.nativeElement.querySelectorAll('.recent-search');
          // this.showInput = true;
        }
        const nextelement = this._ref.nativeElement.querySelectorAll('.li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
        if (nextelement.length !== 0 && nextelement[0].nextElementSibling != null) {
          nextelement[0].classList.remove('li-style-searchhover');
        }
        if (nextelement.length === 0) {
          items[0].classList.add('li-style-searchhover');
          // this.inputtextfield.inputText = this._ref.nativeElement.querySelectorAll('.li-style-searchhover')[0].innerText;
          searchmenu[0].scrollTop = this._ref.nativeElement.querySelectorAll('.li-style-searchhover')[0].offsetTop;
        } else {
          if (nextelement[0].nextElementSibling != null) {
            nextelement[0].nextElementSibling.classList.add('li-style-searchhover');
            // this.inputtextfield.inputText = this._ref.nativeElement.querySelectorAll('.li-style-searchhover')[0].innerText;
            searchmenu[0].scrollTop = this._ref.nativeElement.querySelectorAll('.li-style-searchhover')[0].offsetTop;
          }
        }
      }
    } else if (event.keyCode === 38) {
      // tslint:disable-next-line:max-line-length
      if (this._ref.nativeElement.querySelectorAll('.show-recent-search').length > 0 || this._ref.nativeElement.querySelectorAll('.show-suggestions').length > 0) {
        let items = this._ref.nativeElement.querySelectorAll('.li-style-suggestions') as HTMLCollectionOf<HTMLElement>;
        if (items.length === 0) {
          items = this._ref.nativeElement.querySelectorAll('.li-style-search') as HTMLCollectionOf<HTMLElement>;
          searchmenu = this._ref.nativeElement.querySelectorAll('.recent-search');
          // this.showInput = true;
        }
        const previouselement = this._ref.nativeElement.querySelectorAll('.li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
        if (previouselement.length !== 0 && previouselement[0].previousElementSibling != null) {
          previouselement[0].classList.remove('li-style-searchhover');
        }
        if (previouselement.length === 0) {
          items[0].classList.add('li-style-searchhover');
          //  this.inputtextfield.inputText = this._ref.nativeElement.querySelectorAll('.li-style-searchhover')[0].innerText;
          searchmenu[0].scrollTop = this._ref.nativeElement.querySelectorAll('.li-style-searchhover')[0].offsetTop;
        } else {
          if (previouselement[0].previousElementSibling != null) {
            previouselement[0].previousElementSibling.classList.add('li-style-searchhover');
            // this.inputtextfield.inputText = this._ref.nativeElement.querySelectorAll('.li-style-searchhover')[0].innerText;
            searchmenu[0].scrollTop = this._ref.nativeElement.querySelectorAll('.li-style-searchhover')[0].offsetTop;
          }
        }
      }
    } else if (event.keyCode === 13) {
      this.recentSearchesFunc(this.inputtextfield.inputText);
      this.showInput = false;
      this.filteredList = [];
      this.showDiv1 = false;
      this.selectedItem = true;
      this.initialAnimation = true;
      const selectedelement = this._ref.nativeElement.querySelectorAll('.li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
      if (selectedelement.length !== 0) {
        selectedelement[0].classList.remove('li-style-searchhover');
        selectedelement[0].classList.remove('li-style-searchhover-active');
        this.inputtextfield.inputText = selectedelement[0].innerText;
      }
      const currentsearch = this._ref.nativeElement.querySelectorAll('.li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
      if (currentsearch.length > 0) {
        currentsearch[0].classList.remove('li-style-searchhover');
      }
      this.callBackValue.emit(this.inputtextfield.inputText);
    } else {
      this.itemsInPage = JSON.parse(JSON.stringify(this.copyOfItemsInPage));
      this.showDiv1 = true;
      this.hoverItems = [];
      this.temporaryInput = this.inputtextfield.inputText;
      this.selectedItem = false;
      let count1 = 0;
      let count2 = 0;
      if (this.inputtextfield.inputText !== '') {
        for (const item1 of this.copyOfItemsInPage) {
          if (item1.title === this.inputtextfield.inputText) {
            count1 = 1;
          } else {
            count2 = 1;
          }
        }
      }
      if (this.inputtextfield.inputText !== '' && count1 !== 1) {
        this.filteredList = this.itemsInPage.filter(item => {
          // tslint:disable-next-line:forin
          for (const key in item) {
            // if(key === "title"){
            let comppleteString = '';
            if (('' + item[key]).toLocaleLowerCase().includes(this.inputtextfield.inputText.toLocaleLowerCase())) {
              const index = item[key].toLocaleLowerCase().indexOf(this.inputtextfield.inputText.toLocaleLowerCase());
              this.hoverItems.push(JSON.parse(JSON.stringify( item)));
              const length1 = this.inputtextfield.inputText.toLocaleLowerCase().length;
              const itemKeyCopy = item[key];
              const var2 = item[key].slice(index, index + length1).bold();
              // tslint:disable-next-line:max-line-length
              comppleteString = item[key].slice(0, index) + item[key].slice(index, index + length1).bold() + item[key].slice(index + length1);
              item[key] = comppleteString;
              return true;
            }
            // }
          }
          return false;
        });
      }
      if (this.inputtextfield.inputText === '') {
        // this.selectedIndexEnter = -1;
        const currentsearch = this._ref.nativeElement.querySelectorAll('.li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
        if (currentsearch.length > 0) {
          currentsearch[0].classList.remove("li-style-searchhover");
        }
        this.filteredList = [];
        this.showInput = true;
      }
    }
    if (!this.inputtextfield.inputText) {
      this.callBackValue.emit(this.inputtextfield.inputText);
    }
  }

  select(item, i) {
    const currentsearch = this._ref.nativeElement.querySelectorAll('.li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
    if (currentsearch.length > 0) {
      currentsearch[0].classList.remove('li-style-searchhover');
    }
    if ((this.filteredList.length > 0 && this.inputtextfield.inputText !== '' && this.showrecentSearches) ||
      (this.filteredList.length > 0 && this.showDiv1 && this.inputtextfield.inputText !== '')) {
      if (this.hoverItems.length !== 0) {
        this.inputtextfield.inputText = this.hoverItems[i].title;
      }
    }
  }

  mouseaction() {
    this.filteredList = [];
    this.showDiv1 = false;
    this.selectedItem = false;
    this.showInput = false;
    this.callBackValue.emit(this.inputtextfield.inputText);
  }

  autoCompletesearch(item) {
    // this.inputtextfield.inputText = item;
  }
  selectSearch(item) {

    // alert(item);
    const currentsearch = this._ref.nativeElement.querySelectorAll('.li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
    if (currentsearch.length > 0) {
      currentsearch[0].classList.remove('li-style-searchhover');
    }
    if (this.persistent === true) {
      this.showInput = true;
    }
    this.showInput = true;
    // tslint:disable-next-line:max-line-length
    if ((this.recentSearches.length > 0 && this.inputtextfield.inputText === '' && this.showrecentSearches) || (this.recentSearches.length > 0 && this.showRecentSearchesElements && this.inputtextfield.inputText === '' && this.showInput)) {
      this.inputtextfield.inputText = item;
      let count1 = 0;
      let count2 = 0;

      for (const item1 of this.copyOfItemsInPage) {
        if (item1.title === item) {
          count1 = 1;
        } else {
          count2 = 1;
        }
        this.showDiv1 = false;
        this.filteredList = [];
      }
      if (count1 !== 1) {
        this.inputtextfield.inputText = item;
        // this.filter();
        this.showDiv1 = true;
      }
      this.unshift = true;
      this.recentSearchesFunc(item);
    }
    this.selectedItem = true;
    this.inputtextfield.inputText = item;
    // this.showInput = false;
    // this.callBackValue.emit(this.inputtextfield.inputText);
  }

  mouseactionsearch() {
    this.showInput = false;
    this.callBackValue.emit(this.inputtextfield.inputText);
  }

  ngOnInit() {

    this.copyOfItemsInPage = JSON.parse(JSON.stringify(this.itemsInPage));
    this.inputtextfield = {
      inputText: ''
    };
    this.a = document.getElementById(this.elementId);
    console.log(this.a);
    this.tabIndex = 0;
  }

  autoCompleteInput(item, i) {
    // tslint:disable-next-line:max-line-length
    if (this.filteredList.length > 0 && this.inputtextfield.inputText !== '' && this.showrecentSearches || (this.showDiv1 && this.inputtextfield.inputText !== '' && this.showrecentSearches)) {
      // this.inputtextfield.inputText = this.hoverItems[i].title;
    }
  }

  focusFunction() {
// console.log(this._ref);
//   let a;
//   console.log(this.renderer.selectRootElement('#' + this.elementId));
//     if (!this.persistent) {
//       a = this.renderer.selectRootElement('#' + this.elementId);
    
//     console.log(a);
//     a.focus();
//    //  a.focus();
//     } else if (this.persistent) {
//       a = this.renderer.selectRootElement('#'+ this.elementId);
//       console.log(a);
//       a.focus();
//     }
this.a = document.getElementById(this.elementId);
   this.a.focus();
    const currentsearch = this._ref.nativeElement.querySelectorAll('.li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
    if (currentsearch.length > 0) {
      currentsearch[0].classList.remove('li-style-searchhover');
    }
    if (this.inputtextfield.inputText !== '') {

      this.filteredList = this.itemsInPage.filter(item => {
        // tslint:disable-next-line:forin
        for (const key in item) {
          let comppleteString = '';
          if (('' + item[key]).toLocaleLowerCase().includes(this.inputtextfield.inputText.toLocaleLowerCase())) {
            // tslint:disable-next-line:prefer-const
            let index = item[key].toLocaleLowerCase().indexOf(this.inputtextfield.inputText.toLocaleLowerCase());
            this.hoverItems.push(JSON.parse(JSON.stringify( item)));
            const length1 = this.inputtextfield.inputText.toLocaleLowerCase().length;
            const itemKeyCopy = item[key];
            const var2 = item[key].slice(index, index + length1).bold();
            comppleteString = item[key].slice(0, index) + item[key].slice(index, index + length1).bold() + item[key].slice(index + length1);
            item[key] = comppleteString;
            return true;
          }
        }
        return false;
      });
    }
    if (this.filteredList.length === 0) {
      this.showrecentSearches = true;
      this.showInput = true;
      this.showRecentSearchesElements = true;
      this.filteredList = [];
    //  console.log(this.recentSearches.length);
    }

  // a.focus();
   
  }

  showRecentSearchesDiv() {
    if (this.showrecentSearches) {
      this.showRecentSearchesElements = true;
    }
  }

  hideRecentSearchesDiv() {
   this.showRecentSearchesElements = false;
  }

  showSuggestionsDiv() {
    this.showSuggestionsElements = true;
  }

  hideSuggestionsDiv() {
    this.showSuggestionsElements = false;
  }

  search() {
    if (this.searchField.nativeElement.offsetWidth === 0) {
      this.placeholder = 'Search';
      this.showInput = true;
      this.initialAnimation = true;
      this.inputtextfield.inputText = '';
      if (this.showInput === true) {
       // const a  = this.renderer.selectRootElement('#' + this.elementId);
        this.a.focus();
      }
    }
  }

  clearData() {
    this.recentSearchesFunc(this.inputtextfield.inputText);
    this.inputtextfield.inputText = '';
    if (!this.persistent) {
      //const a =  this.renderer.selectRootElement('#' + this.elementId);
      this.a.focus();
    } else if (this.persistent) {
     // const a = this.renderer.selectRootElement('#' + this.elementId);
      this.a.focus();
    }
    const currentsearch = this._ref.nativeElement.querySelectorAll('.li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
    if (currentsearch.length > 0) {
      currentsearch[0].classList.remove('li-style-searchhover');
    }
    this.filteredList = [];
    this.showInput = true;
  }
  temporaryInputFunc() {

    if (!this.selectedItem) {
      this.inputtextfield.inputText = this.temporaryInput;
    } else {
      this.temporaryInput = '';
    }
  }
}
// tslint:disable-next-line:class-name
export class inputtextfield {
  inputText: string;

}
