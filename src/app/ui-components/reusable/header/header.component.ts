import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { CommonService } from '../../../services/common.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private _location: Location, public commonService: CommonService) { }
  @Input() visible = true;
  ngOnInit() {
  }

  gotopreviouspage() {
    this.commonService.showHeader();
    window.history.back();
  }
  contactUs() {
    // logic on click of phone icon
  }
}
