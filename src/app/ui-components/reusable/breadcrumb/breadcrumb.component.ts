import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  breadcrumbvalue = [
    {
      path: 'family-allowancefa-controls', url: '/family-allowance/fa-controls',
      description: 'Home'
    },
    {
      path: 'family-allowancefa-dashboard', url: '/family-allowance/fa-controls',
      description: 'Home/Family allowances dashboard'
    },
    {
      path: 'family-allowancefa-lines', url: '/family-allowance/fa-controls',
      description: 'Home/Family allowances line'
    },
    {
      path: 'family-allowancefa-lines-history', url: '/family-allowance/fa-controls',
      description: 'Home/Family allowances line history'
    },
    {
      path: 'family-allowancefa-dashboard-singleline', url: '/family-allowance/fa-controls',
      description: 'Home/Family allowances dashboard singleline'
    }
  ];


  currenturl: string;
  currentbreadcrumb;
  currentpathdata = [];
  currentbreadcrumbpath = [];
  currentpath;
  constructor(private router: Router, public commonService: CommonService) { }
  ngOnInit(): void {
    // tslint:disable-next-line:prefer-const
    let currentpath;
    let currentdesc;
    const subscription = this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {

        this.currenturl = event.url;
        this.currentpathdata = [];
        this.currentbreadcrumbpath = [];
        if (this.currenturl !== '/' || this.currenturl.length === 0) {
          this.currentbreadcrumb = this.breadcrumbvalue.find(x => x.path === this.currenturl.replace(/\//g, ''));
          if (this.currentbreadcrumb != null) {
            this.currentpath = this.currentbreadcrumb['description'].split('/');
            let currentdata = this.currentbreadcrumb['description'].toString();
            while (currentdata.length !== 0) {
              currentdesc = this.breadcrumbvalue.find(x => x.description === currentdata.toString());
              this.currentpathdata.push(currentdesc);
              if (currentdata.lastIndexOf('/') !== -1) {
                currentdata = currentdata.substring(0, currentdata.lastIndexOf('/'));
              } else {
                currentdata = '';
              }
            }
            this.currentbreadcrumbpath = this.currentpathdata;
          }
        }
      }
    });

  }
}


