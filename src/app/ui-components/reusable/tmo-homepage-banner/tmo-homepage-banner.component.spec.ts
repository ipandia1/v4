import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmoHomepageBannerComponent } from './tmo-homepage-banner.component';

describe('TmoHomepageBannerComponent', () => {
  let component: TmoHomepageBannerComponent;
  let fixture: ComponentFixture<TmoHomepageBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmoHomepageBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmoHomepageBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
