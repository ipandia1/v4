import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-tmo-homepage-banner',
  templateUrl: './tmo-homepage-banner.component.html',
  styleUrls: ['./tmo-homepage-banner.component.scss']
})
export class TmoHomepageBannerComponent implements OnInit {
  @Input() imageAlignment;
  @Input() ctaAlignment;
  @Input() totalItems;

  constructor() {
  }
  ngOnInit() {

  }
}
