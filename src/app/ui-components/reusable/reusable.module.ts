import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Reusable Module
import { ReusableRoutingModule } from './reusable-routing.module';

// Common Reusable Component

import { ProfileBladeComponent } from './profile-blade/profile-blade.component';
import { SearchReusableComponent } from './search-reusable/search-reusable.component';
import { DropdownSelectorComponent } from './dropdown-selector/dropdown-selector.component';
import {BreadcrumbComponent} from './breadcrumb/breadcrumb.component';
import {SpinnerComponent} from './spinner/spinner.component';
import { GlobalHeaderComponent } from './global-header/global-header.component';
import { GlobalFooterComponent } from './global-footer/global-footer.component';
import { NotificationComponent } from './notification/notification.component';
import { GlobalDropdownComponent } from './global-dropdown/global-dropdown.component';
// Directive
import { ButtonrippleDirective } from '../../directives/buttonripple.directive';
import { CustomformatDirective } from '../../directives/customformat.directive';
import { DropdownDirective } from '../../directives/dropdown.directive';
import {CheckBoxDirective} from '../../directives/checkbox.directive';
import { GlobalDropdownDirective } from '../../directives/global-dropdown.directive';
import { GlobalHeaderDropdownDirective } from '../../directives/global-header-dropdown.directive';
// Pipes

import { from } from 'rxjs/internal/observable/from';
import { TmoHomepageBannerComponent } from './tmo-homepage-banner/tmo-homepage-banner.component';
@NgModule({
  imports: [ // Add related entities in alphabetical Order
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ReusableRoutingModule
  ],
  declarations: [ // Add related entities in alphabetical Order
    // reusable
    ProfileBladeComponent,
    SearchReusableComponent,
    DropdownSelectorComponent,
    BreadcrumbComponent,
    GlobalHeaderComponent,
    GlobalFooterComponent,
    SpinnerComponent,
    NotificationComponent,
    // directives
    ButtonrippleDirective,
    CustomformatDirective,
    DropdownDirective,
    CheckBoxDirective,
    GlobalDropdownDirective,
    GlobalDropdownComponent,
    GlobalHeaderDropdownDirective,
    TmoHomepageBannerComponent
  ],
  exports: [ // Add related entities in alphabetical Order
    FormsModule,
    ReactiveFormsModule,
    // reusable
    SearchReusableComponent,
    ProfileBladeComponent,
    DropdownSelectorComponent,
    BreadcrumbComponent,
    GlobalHeaderComponent,
    GlobalFooterComponent,
    SpinnerComponent,
    NotificationComponent,
    // directives
    ButtonrippleDirective,
    CustomformatDirective,
    DropdownDirective,
    CheckBoxDirective,
    GlobalDropdownDirective,
    GlobalDropdownComponent,
    GlobalHeaderDropdownDirective,
    TmoHomepageBannerComponent
  ]
})
export class ReusableModule { }
