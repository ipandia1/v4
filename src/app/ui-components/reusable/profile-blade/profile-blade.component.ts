import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-blade',
  templateUrl: './profile-blade.component.html',
  styleUrls: ['./profile-blade.component.scss']
})
export class ProfileBladeComponent implements OnInit {
  @Input() titleText: string;
  @Input() titleDesc: string;
  @Input() titleDesc2: string;
  @Input() url = '';
  @Input() topDivider = true;
  @Input() topDividerClass = 'inset-divider-1px';
  @Input() bottomDivider = true;
  @Input() bottomDividerClass = '';
  @Input() titleTextClass = 'display6';
  @Input() titleDescClass = 'body-copy d-inline-block';
  @Input() titleDesc2Class = 'body-copy';
  @Input() icon = 'arrow-right d-inline-block';
  @Input() switch = false;
  @Input() checkOpt = false;
  @Input() moreLessDiv = false;
  @Input() showMore = false;
  @Input() ShowMoreText = 'Show more';
  @Input() ShowLessText = 'Show less';
  @Input() ShowTextClass = 'body-copy brand-magenta';
  emptyValue = '';
  show: boolean;
  constructor(private router: Router) { }

  ngOnInit() {
  }
  reDirectTo(ev, url) {
    if (ev.keyCode === 13) {
      this.router.navigate([url]);
    }
  }
}
