import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, HostListener } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],

  animations: [
  trigger('alertAnimation', [
    transition('void => *', [
      style({transform: 'translateY(-100%)'}),
      animate(500)
    ]),
    transition('* => void', [
      animate(500, style({transform: 'translateY(-100%)'}))
      ])
  ])
]
})
export class NotificationComponent implements OnInit {
  @Input('alertIcon') alertIcon = 'error_filled_white';
  @Input('alertType') alertType = 'Critical';
  @Input('alertDesc') alertDesc = 'Here\'s a strong alert headline';
  @Input('alertSubDesc') alertSubDesc = 'Here\'s some additional copy explaining how to resolve';
  @Input('alertSubDescLink') alertSubDescLink = '';
  @Input('alertSubDescMobile') alertSubDescMobile = '';
  @Input('alertNavIcon') alertNavIcon = 'arrow-right-white';
  @Input('alertResolveURL') alertResolveURL = '';
  @Input('alertResolveURLAnchor') alertResolveURLAnchor = '';
  @Input('showNotification') showNotification = true;
  @Input('backgroundColor') backgroundColor = '#000000';
  @Output('callBackCloseAlert') callBackCloseAlert = new EventEmitter<boolean>();
  @ViewChild('stickyn') sticky: ElementRef;
  isEmpty = '';
  // stickTop: boolean;
  // alertBarOffset: any;
  headerTop = true;

  constructor(private router: Router, private commonService: CommonService) { }
  @HostListener('window:resize', [])
  onResize() {
    window.scrollTo(0, 0);
    const checkApp = document.querySelector('.hd-close-pink');
    if (!checkApp) {
      this.headerTop = false;
    } else { this.headerTop = true; }
  }

  @HostListener('document:click', ['$event.target'])
  onClick(event) {
    const checkApp = document.querySelector('.hd-close-pink');
    if (!checkApp) {
      this.headerTop = false;
    }
  }
  ngOnInit() {
    if (this.commonService.visibleTmoHeader) {
      const checkApp = document.querySelector('.hd-close-pink');
      if (!checkApp) {
        this.headerTop = false;
      } else { this.headerTop = true; }
    } else { this.headerTop = false; }
  }
  resolveAlert() {
    if ((this.alertResolveURL !== '' && this.alertResolveURLAnchor !== '')) { // Navigate to URl and its anchor tag
      this.router.navigate([this.alertResolveURL], { fragment: this.alertResolveURLAnchor });
    } else if (this.alertResolveURL !== '') { // Navigate to URL
      this.router.navigate([this.alertResolveURL]);
    } else {
      this.showNotification = !this.showNotification;
      this.callBackCloseAlert.emit(this.showNotification);
    }
  }
}
