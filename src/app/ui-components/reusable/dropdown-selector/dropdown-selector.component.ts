import { Component, OnInit, Input,  Output, EventEmitter, } from '@angular/core';
// import { Output } from '@angular/core/src/metadata/directives';

@Component({
  selector: 'app-dropdown-selector',
  templateUrl: './dropdown-selector.component.html',
  styleUrls: ['./dropdown-selector.component.scss']
})
export class DropdownSelectorComponent implements OnInit {
  @Input() selectedVal = 'Select State';
  @Input() dropdownidval;
  @Input() dropdownArray;
  @Input() dropdownText;
  @Input() tabindex;
  @Input() disabled;
  // tslint:disable-next-line:no-output-rename
  @Output('callBackValue') callBackValue = new EventEmitter<string>();
  select(value: string): void {
    this.selectedVal = value;
    this.callBackValue.emit(value);
  }
  constructor() {}

  ngOnInit() {
    
      this.tabindex = 0;
    
  }
}
