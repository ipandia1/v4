import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-download',
  templateUrl: './tmo-app-download.component.html',
  styleUrls: ['./tmo-app-download.component.scss']
})
export class TmoappdownloadComponent implements OnInit {
  isTmoHeader = false;
  count = 0;
  @ViewChild('stickyd') sticky: ElementRef;

  constructor(public commonService: CommonService) { }

  ngOnInit() {
    if (window.innerWidth < 576) {
      this.isTmoHeader = true;
    } else {
      this.isTmoHeader = false;
    }
  }

  @HostListener('window : resize', [])
  onresize() {
    window.scrollTo(0, 0);
    if (window.innerWidth < 576) {
      if (this.count === 1) {
        this.isTmoHeader = false;
      } else {
        this.isTmoHeader = true;
      }
    } else {
      this.isTmoHeader = false;
    }
  }

  closeHeader() {
    this.count = 1;
    this.isTmoHeader = false;
  }

  getBtn() {
    console.log('clicked on Get button');
  }
}
