import { Component, OnInit, HostListener, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../../services/common.service';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-global-header',
  templateUrl: './global-header.component.html',
  styleUrls: ['./global-header.component.scss']
})
export class GlobalHeaderComponent implements OnInit, AfterViewInit {

  dropdownidval = 'globalDropdown';
  language = 'Español';
  selectedVal = 'Eric';
  showInput = false;
  mobileView = false;
  visible = false;
  placeholder = 'Search';
  showImages = true;
  showClose = false;
  inputtext1 = '';
  showPersonWhite = true;
  searchImg = 'gh-search-white';
  profileName = 'Eliza Miranda Rosemary Gabriella';
  magentaIconsArray = ['gh-call-magenta', 'gh-comments-magenta'];
  iconArray = [];
  showCallIconHover = true;
  showMessageIconHover = true;
  SelectDataPath = 'assets/json/searchData.json';
  filteredList = [];
  totalItems = [];
  copyOfItemsInPage = [];
  itemsInPage = [];
  hoverItems = [];
  recentSearches = [];
  showrecentSearches = false;
  unshift = false;
  selecteditem = false;

  @ViewChild('searchcomp') search: ElementRef;
  navItems = [
    { title: 'Bill', url: '', image: 'gh-call-white', alt: 'call us' },
    { title: 'Usage', url: '', image: 'gh-comments-white', alt: 'message us' },
    { title: 'Account', url: '' },
    { title: 'Phone', url: '' },
    { title: 'Shop', url: '' }];
  detailsArray = [
    { title: 'Profile', url: '' },
    { title: 'Account history', url: '' },
    { title: 'Log out', url: '/' }];

  @ViewChild('sticky') sticky: ElementRef;
  headerOffset: any;
  headerTop = true;

  constructor(private router: Router, public elref: ElementRef, private commonService: CommonService) {
    this.commonService.getJsonData(this.SelectDataPath)
      .subscribe(responseJson => {
        this.totalItems = responseJson['itemsInPage'];
        for (let i = 0; i < this.totalItems.length; i++) {
          this.itemsInPage.push(this.totalItems[i]);
          this.copyOfItemsInPage.push(this.totalItems[i]);
        }
      });
  }

  ngOnInit() {
    this.mobileDetect();
    for (let i = 0; i < this.navItems.length; i++) {
      if (this.navItems[i].image) {
        this.iconArray.push({ 'image': this.navItems[i].image, 'alt': this.navItems[i].alt });
      }
    }
  }

  ngAfterViewInit() {
    this.headerOffset = this.sticky.nativeElement.offsetTop;
    if (this.commonService.visibleTmoHeader) {
      this.headerTop = true;
    } else { this.headerTop = false; }
  }

  @HostListener('blur', ['$event'])
  hideNav(event) {
    if (this.elref.nativeElement !== event.target) {
      this.closeNav();
    }
  }

  @HostListener('keyup', ['$event'])
  onKeyupHandler(event: KeyboardEvent) {
    const keyCode = event.keyCode;
    if (keyCode === 13) {
      const activeSearch1 = document.activeElement.classList.contains('gh-search-white');
      const activeSearch2 = document.activeElement.classList.contains('gh-search-gray');
      if (activeSearch1) {
        this.openSearch();
      } else if (activeSearch2) {
        this.closeSearch();
      }
      const activeSearch4 = document.activeElement.classList.contains('gh-search-magenta');
      if (activeSearch4) {
        this.selecteditem = true;
        this.onSearch();
      }
    }
    event.preventDefault();
  }

  @HostListener('document:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    const activeSearch3 = document.activeElement.classList.contains('gh-search-magenta');
    const activeSearch2 = document.activeElement.classList.contains('gh-search-gray');
    if (activeSearch3 || activeSearch2) {
      this.selecteditem = true;
      this.showrecentSearches = false;
      this.filteredList = [];
    }
  }

  @HostListener('window:load', [])
  onWindowLoad() {
    this.mobileDetect();
  }

  @HostListener('window:resize', [])
  onResize() {
    this.mobileDetect();
    window.scrollTo(0, 0);
    const checkApp = document.querySelector('.hd-close-pink');
    if (!checkApp) {
      this.headerTop = false;
    } else { this.headerTop = true; }
  }

  mobileDetect() {
    if (window.innerWidth < 768) {
      this.mobileView = true;
      if ((this.showInput && this.inputtext1 === '') || this.inputtext1 !== '') {
        this.visible = true;
        document.getElementById('mobileSearchId').focus();
        if (this.showInput || document.getElementById('sidenavID').clientHeight > 0) {
          const body = document.getElementsByTagName('body')[0];
          body.classList.add('noScroll');
        }
      }
    } else {
      this.mobileView = false;
      this.visible = false;
      const body = document.getElementsByTagName('body')[0];
      body.classList.remove('noScroll');
      if (this.showInput) {
        this.showImages = false;
        document.getElementById('searchId').focus();
      }
    }
  }
  openNav() {
    document.getElementById('sidenavID').setAttribute('style', 'width:272px; border-top: 6px solid #e20074;');
    this.showClose = true;
    const openDropdown = this.elref.nativeElement.querySelector('#sidenavID');
    openDropdown.setAttribute('aria-hidden', false);
  }

  closeNav() {
    document.getElementById('sidenavID').setAttribute('style', ' width:0px;');
    this.showClose = false;
    const openDropdown = this.elref.nativeElement.querySelector('#sidenavID');
    openDropdown.setAttribute('aria-hidden', true);
  }
  select(value) {
    this.selectedVal = value;
    for (let i = 0; i < this.detailsArray.length; i++) {
      if (this.selectedVal === this.detailsArray[i].title) {
        this.router.navigate([this.detailsArray[i].url]);
        document.getElementById('sidenavID').setAttribute('style', 'display: none');
      }
    }
  }
  openSearch() {
    this.showInput = true;
    this.inputtext1 = '';
    this.placeholder = 'Search';
    if (this.mobileView) {
      this.visible = true;
      setTimeout(() => document.getElementById('searchId').focus(), 0);
      const body = document.getElementsByTagName('body')[0];
      body.classList.add('noScroll');
    } else {
      this.visible = false;
      this.showImages = false;
      setTimeout(() => document.getElementById('searchId').focus(), 0);
    }
  }
  closeSearch() {
    this.searchImg = 'gh-search-white';
    this.showInput = false;
    this.showImages = true;
    this.visible = false;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
    this.inputtext1 = '';
    if (!this.mobileView) {
      setTimeout(() => document.getElementById('searchIconId').focus(), 0);
    }
  }

  onSearch() {
    this.closeSearch();
    //console.log('clicked on search');
  }

  hidekeydown(event) {
    if (event.keyCode === 40) {
      event.preventDefault();
    }
  }
  changeIcon(val) {
    if (val === 'search') {
      this.searchImg = 'gh-search-magenta';
    } else if (val === 'dropdown') {
      this.showPersonWhite = false;
    } else if (val === 'callicon') {
      this.showCallIconHover = false;
    } else if (val === 'messageicon') {
      this.showMessageIconHover = false;
    } else {
      this.iconArray[val].image = this.magentaIconsArray[val];
    }
  }

  changeMagenta(val) {
    if (val === 'search') {
      this.searchImg = 'gh-search-white';
    } else if (val === 'dropdown') {
      this.showPersonWhite = true;
    } else if (val === 'callicon') {
      this.showCallIconHover = true;
    } else if (val === 'messageicon') {
      this.showMessageIconHover = true;
    } else {
      this.iconArray[val].image = this.navItems[val].image;
    }
  }

  /* Search functionality starts */
  filter(event: any) {
    this.selecteditem = false;
    let searchmenu = this.elref.nativeElement.querySelectorAll('.gh-search-menu');
    if (event.keyCode === 40) {
      if (this.elref.nativeElement.querySelectorAll('.gh-show-recent-search').length > 0 ||
        this.elref.nativeElement.querySelectorAll('.gh-show-suggestions').length > 0) {
        let items = this.elref.nativeElement.querySelectorAll('.gh-li-style-suggestions') as HTMLCollectionOf<HTMLElement>;
        if (items.length === 0) {
          items = this.elref.nativeElement.querySelectorAll('.gh-li-style-search') as HTMLCollectionOf<HTMLElement>;
          searchmenu = this.elref.nativeElement.querySelectorAll('.gh-recent-search');
        }

        const nextelement = this.elref.nativeElement.querySelectorAll('.gh-li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
        if (nextelement.length !== 0 && nextelement[0].nextElementSibling != null) {
          nextelement[0].classList.remove('gh-li-style-searchhover');
          const i = nextelement[0].nextElementSibling.getAttribute("value");
          document.getElementById('searchId').setAttribute('aria-activedescendant', '0');
          nextelement[0].removeAttribute('aria-label');
        }
        if (nextelement.length === 0) {
          items[0].classList.add('gh-li-style-searchhover');
          const nextOption = items[0].innerHTML.replace(/<\/?[^>]+(>|$)/g, '');
          document.getElementById('searchId').setAttribute('aria-activedescendant', '0');
          items[0].setAttribute('aria-label', nextOption);

        } else {
          if (nextelement[0].nextElementSibling != null) {
            const nextOption = nextelement[0].nextElementSibling.innerHTML.replace(/<\/?[^>]+(>|$)/g, '');
            nextelement[0].nextElementSibling.classList.add('gh-li-style-searchhover');
            searchmenu[0].scrollTop = this.elref.nativeElement.querySelectorAll('.gh-li-style-searchhover')[0].offsetTop;
            const i = nextelement[0].nextElementSibling.getAttribute("value");
            document.getElementById('searchId').setAttribute('aria-activedescendant', i);
            nextelement[0].nextElementSibling.setAttribute('aria-label', nextOption);

          }
        }
      }
    } else if (event.keyCode === 38) {
      if (this.elref.nativeElement.querySelectorAll('.gh-show-recent-search').length > 0 ||
        this.elref.nativeElement.querySelectorAll('.gh-show-suggestions').length > 0) {
        let items = this.elref.nativeElement.querySelectorAll('.gh-li-style-suggestions') as HTMLCollectionOf<HTMLElement>;
        if (items.length === 0) {
          items = this.elref.nativeElement.querySelectorAll('.gh-li-style-search') as HTMLCollectionOf<HTMLElement>;
          searchmenu = this.elref.nativeElement.querySelectorAll('.gh-recent-search');
        }
        const previouselement = this.elref.nativeElement.querySelectorAll('.gh-li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
        if (previouselement.length !== 0 && previouselement[0].previousElementSibling != null) {
          previouselement[0].classList.remove('gh-li-style-searchhover');
          previouselement[0].removeAttribute('aria-label');
        }
        if (previouselement.length === 0) {
          items[0].classList.add('gh-li-style-searchhover');
          this.inputtext1 = this.elref.nativeElement.querySelectorAll('.gh-li-style-searchhover')[0].innerText;
          searchmenu[0].scrollTop = this.elref.nativeElement.querySelectorAll('.gh-li-style-searchhover')[0].offsetTop;
          const prevOption = items[0].previousElementSibling.innerHTML.replace(/<\/?[^>]+(>|$)/g, '');
          const i = items[0].previousElementSibling.getAttribute("value");
          document.getElementById('searchId').setAttribute('aria-activedescendant', i);
          items[0].setAttribute('aria-label', prevOption);
        } else {
          if (previouselement[0].previousElementSibling != null) {
            previouselement[0].previousElementSibling.classList.add('gh-li-style-searchhover');
            searchmenu[0].scrollTop = this.elref.nativeElement.querySelectorAll('.gh-li-style-searchhover')[0].offsetTop;
            const prevOption = previouselement[0].previousElementSibling.innerHTML.replace(/<\/?[^>]+(>|$)/g, '');
            const i = previouselement[0].previousElementSibling.getAttribute("value");
            document.getElementById('searchId').setAttribute('aria-activedescendant', i);
            previouselement[0].previousElementSibling.setAttribute('aria-label', prevOption);

          }
        }
      }

    } else if (event.keyCode === 13) {
      this.filteredList = [];
      if (this.mobileView) {
        document.getElementById('mobileSearchId').focus();
      } else {
        document.getElementById('searchId').focus();
      }
      const selectedelement = this.elref.nativeElement.querySelectorAll('.gh-li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
      this.showrecentSearches = false;
      if (selectedelement.length !== 0) {
        selectedelement[0].classList.remove('gh-li-style-searchhover');
        selectedelement[0].classList.remove('li-style-searchhover-active');
        this.inputtext1 = selectedelement[0].innerText;
        this.recentSearchesFunc(this.inputtext1);
      }
      this.recentSearchesFunc(this.inputtext1);
      this.hideRecentSearchesDiv();
      const currentsearch = this.elref.nativeElement.querySelectorAll('.gh-li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
      if (currentsearch.length > 0) {
        currentsearch[0].classList.remove('gh-li-style-searchhover');
      }
    } else {
      this.itemsInPage = JSON.parse(JSON.stringify(this.copyOfItemsInPage));
      this.hoverItems = [];
      let count1 = 0;
      let count2 = 0;
      if (this.inputtext1 !== '') {
        for (const item1 of this.copyOfItemsInPage) {
          if (item1.title === this.inputtext1) {
            count1 = 1;
          } else {
            count2 = 1;
          }
        }
      }
      if (this.inputtext1 !== '' && count1 !== 1) {
        this.filteredList = this.itemsInPage.filter(item => {
          for (const key in item) {
            if (key === 'title') {
              let completeString = '';
              if (('' + item[key]).toLocaleLowerCase().includes(this.inputtext1.toLocaleLowerCase())) {
                const index = item[key].toLocaleLowerCase().indexOf(this.inputtext1.toLocaleLowerCase());
                this.hoverItems.push(JSON.parse(JSON.stringify(item)));
                const length1 = this.inputtext1.toLocaleLowerCase().length;
                const itemKeyCopy = item[key];
                const var2 = item[key].slice(index, index + length1).bold();
                completeString = itemKeyCopy.slice(0, index) + var2 + itemKeyCopy.slice(index + length1);
                item[key] = completeString;
                return true;
              }
            }
          }
          return false;
        });
      }
      if (this.filteredList.length === 0) {
        this.selecteditem = false;
        this.showrecentSearches = true;
      }
      if (this.inputtext1 === '') {
        this.filteredList = [];
        this.showrecentSearches = true;
        this.showRecentSearchesDiv();
        this.showInput = true;
      }
    }
  }
  focusFunction() {
    if (this.inputtext1 !== '') {
      this.filteredList = this.itemsInPage.filter(item => {
        for (const key in item) {
          if (key === 'title') {
            let completeString = '';
            if (('' + item[key]).toLocaleLowerCase().includes(this.inputtext1.toLocaleLowerCase())) {
              const index = item[key].toLocaleLowerCase().indexOf(this.inputtext1.toLocaleLowerCase());
              this.hoverItems.push(JSON.parse(JSON.stringify(item)));
              const length1 = this.inputtext1.toLocaleLowerCase().length;
              const itemKeyCopy = item[key];
              const var2 = item[key].slice(index, index + length1).bold();
              completeString = itemKeyCopy.slice(0, index) + var2 + itemKeyCopy.slice(index + length1);
              item[key] = completeString;
              return true;
            }
          }
        }
        return false;
      });
    }
    if (this.filteredList.length === 0) {
      if (this.selecteditem === true) {
        this.showrecentSearches = false;
        this.selecteditem = false;
      } else {
        this.showrecentSearches = true;
      }
      this.showInput = true;
      this.filteredList = [];
    }
  }

  @HostListener('document:click', ['$event.target'])
  onClick(event) {
    const checkApp = document.querySelector('.hd-close-pink');
    if (!checkApp) {
      this.headerTop = false;
    }
    if ((!this.search.nativeElement.contains(event) && event.classList.contains('gh-clear-icon') === false) ||
      (!this.search.nativeElement.contains(event) && event.classList.contains('gh-search-gray-out') === false)) {
      this.selecteditem = true;
      this.showrecentSearches = false;
      this.filteredList = [];
    }
  }

  hideRecent() {
    this.showrecentSearches = false;
    if (this.inputtext1 === '') {
      this.closeSearch();
    } else {
      this.showInput = true;
    }
  }


  inputClear() {
    this.recentSearchesFunc(this.inputtext1);
    if (this.mobileView) {
      document.getElementById('mobileSearchId').focus();
    } else {
      document.getElementById('searchId').focus();
    }

    const currentsearch = this.elref.nativeElement.querySelectorAll('.gh-li-style-searchhover') as HTMLCollectionOf<HTMLElement>;
    if (currentsearch.length > 0) {
      currentsearch[0].classList.remove('gh-li-style-searchhover');
    }
    this.inputtext1 = '';
    this.filteredList = [];
    this.selecteditem = false;
  }
  showRecentSearchesDiv() {
    this.showrecentSearches = true;
  }

  hideRecentSearchesDiv() {
    this.showrecentSearches = false;
  }
  recentSearchesFunc(value) {
    if (value !== '' && this.recentSearches.indexOf(value) === -1) {
      this.recentSearches.unshift(value);
    }
    if ((this.unshift && this.recentSearches.length > 1) || (value !== '' && this.recentSearches.indexOf(value) !== -1)) {
      // tslint:disable-next-line:prefer-const
      let index = this.recentSearches.indexOf(value);
      if (index !== -1) {
        this.recentSearches.splice(index, 1);
      }
      this.recentSearches.unshift(value);
      this.unshift = false;
    }

  }
  selectSearch(item) {
    this.recentSearchesFunc(item);
    this.filteredList = [];
    this.inputtext1 = item;
    this.showrecentSearches = false;
    this.selecteditem = true;
    if (this.mobileView) {
      setTimeout(() => document.getElementById('mobileSearchId').focus(), 0);
    } else {
      setTimeout(() => document.getElementById('searchId').focus(), 0);
    }
  }
  selectSearchDropdown(i) {
    if (this.filteredList.length > 0 && this.inputtext1 !== '') {
      if (this.hoverItems.length !== 0) {
        this.inputtext1 = this.hoverItems[i].title;
      }
    }
    this.recentSearchesFunc(this.inputtext1);
    this.showrecentSearches = false;
    this.filteredList = [];
    this.selecteditem = true;
    if (this.mobileView) {
      setTimeout(() => document.getElementById('mobileSearchId').focus(), 0);
    } else {
      setTimeout(() => document.getElementById('searchId').focus(), 0);
    }
  }
  /* Search functionality ends */
}
