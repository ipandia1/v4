import { Injectable, ElementRef } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Router, NavigationEnd } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable()
export class CommonService {

  visibleHeader = false;
  visibleBreadcrumb = true;
  showOverlay = true;
  showLoader = true;
  timervalue = 0;
  visibleTmoHeader ;
  visibleGlobalheader = false;
  visibleGlobalfooter = false;
  constructor(private http: Http) {
    this.visibleHeader = false;
    this.visibleBreadcrumb = true;
    this.showOverlay = false;
    this.showLoader = false;
    this.timervalue = 0;
    this.visibleGlobalheader = false;
    this.visibleGlobalfooter = false;
    this.visibleTmoHeader = true;
  }
  /* To enable or disable body scroll */
  showScroll(enableScroll: boolean) {
    const body = document.getElementsByTagName('body')[0];
  // const body = this.renderer.selectRootElement('body',true);
    if (enableScroll === true) {
      body.classList.remove('noScroll');
     // this.renderer.removeClass(body,'noScroll');
    } else if (enableScroll === false) {
      body.classList.add('noScroll');
    // this.renderer.addClass(body,'noScroll');
    }
  }

  /* To get JSON Data */
  getJsonData(filePath: string) {
    return this.http.get(filePath)
      .pipe(map((response: Response) => response.json()));
  }
  /*Go to Anchor Tag in a Page */
  goToAnchorTag(router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const urlTree = router.parseUrl(router.url);
        if (urlTree.fragment) {
          const element =  document.querySelector('#' + urlTree.fragment);
          if (element) {
            element.scrollIntoView(true);
          }
        }
      }
    });
  }

  hideGlobalHeader() {
    this.visibleGlobalheader = false;
  }

  showGlobalHeader() {
    this.visibleGlobalheader = true;
  }

  hideGlobalFooter() {
    this.visibleGlobalfooter = false;
  }

  showGlobalFooter() {
   this.visibleGlobalfooter = true;
  }


  hideHeader() {
    this.visibleHeader = false;
  }

  showHeader() {
   // this.visibleHeader = true;
  }

  hideBreadcrumb() {
    this.visibleBreadcrumb = false;
  }

  showBreadcrumb() {
    this.visibleBreadcrumb = true;
  }

  showSpinner(showspinner, showoverlay, timervalue) {
    this.showLoader = showspinner;
    this.showOverlay = showoverlay;
    this.timervalue = timervalue;
    this.showScroll(false);
    if (this.showLoader === true && timervalue > 0) {
      setTimeout(() => {
        this.hideSpinner();
        this.showScroll(true);
      }, timervalue);
    }
  }
  hideSpinner() {
    this.showLoader = false;
    this.showOverlay = false;
    this.timervalue = 0;
    this.showScroll(true);
  }

  hideTmoHeader() {
    this.visibleTmoHeader = false;
  }

  showTmoHeader() {
    this.visibleTmoHeader = true;
  }
}
