import { Component, OnInit, ViewEncapsulation, Renderer2 } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { PlatformLocation } from '@angular/common';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { CommonService } from './services/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  constructor(location: PlatformLocation, private router: Router, private commonService: CommonService,
    private renderer: Renderer2) {
    location.onPopState(() => {
    });

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      } else if (evt instanceof NavigationEnd) {
        // this.commonService.showHeader();
        // this.commonService.showBreadcrumb();
        // this.commonService.hideGlobalHeader();
        // this.commonService.hideGlobalFooter();
      }
      window.scrollTo(0, 0);
      if (this.commonService.visibleGlobalheader === true) {
        const el = this.renderer.selectRootElement('#gh-tab-focus', true);
        if (el !== null || el !== undefined) {
          el.focus();
        }
      }
    });
  }
  ngOnInit() {
    this.commonService.showGlobalHeader();
    this.commonService.showGlobalFooter();
  }

}
