import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { GridComponent } from './ui-components/grid/grid.component';
import { HeaderComponent } from './ui-components/reusable/header/header.component';
import {TmoappdownloadComponent} from './ui-components/reusable/tmo-app-download/tmo-app-download.component';
import {SchduledCallComponent} from './pages/schduledcall/schduled_call/schduled_call.component';
// import {GlobalFooterComponent} from './ui-components/reusable/global-footer/global-footer.component';
// Reusable Module
import { ReusableModule } from './ui-components/reusable/reusable.module';
// Service imports
import { CommonService } from './services/common.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GridComponent,
    HeaderComponent,
    TmoappdownloadComponent,
    SchduledCallComponent
    // GlobalFooterComponent
  ],
  imports: [
    routing,
    CommonModule,
    FormsModule,
    HttpModule,
    ReusableModule,
    BrowserAnimationsModule,
  ],
  providers: [CommonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
