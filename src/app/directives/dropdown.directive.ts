import { Directive, ElementRef, HostListener, EventEmitter, Input, Output, ViewChild,Renderer2 } from '@angular/core';

import { element } from 'protractor';
@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  @Input() dropdownid: string;
  private Inside = false;
  kcode = 0;
  private accessibility = 'true';
  constructor(private elementref: ElementRef, private renderer: Renderer2) {
  }
  @HostListener('click', ['$event.target'])
  public onclick(target) {
    if (this.dropdownid === 'globalDropdown') {
      const checkClass = target.classList.contains('dropdown-header');
      if (checkClass || this.kcode === 13) {
        const selecteddropdown = this.elementref.nativeElement.children[1];
        this.elementref.nativeElement.focus();
        const dropdownvalue = this.elementref.nativeElement.children[0];
        const bodyrect = selecteddropdown.getBoundingClientRect();
        const activeelement = selecteddropdown.querySelectorAll('.active');
        if (selecteddropdown.classList.toggle('show')) {
        this.renderer.setStyle(selecteddropdown, 'marginTop', -(this.elementref.nativeElement.clientHeight - 7) + 'px');
        this.renderer.setStyle(selecteddropdown, 'top', 0 + 'px');
        this.renderer.setStyle(selecteddropdown, 'borderTop', '4px solid #e20074');
        }
        if (activeelement.length !== 0) {
          (<HTMLElement>selecteddropdown).scrollTop = (<HTMLElement>activeelement[0]).offsetTop;
          this.accessibility === 'true' ? this.accessibility = 'false' : this.accessibility = 'true';
          this.renderer.setAttribute(selecteddropdown , 'aria-hidden', this.accessibility);
        }
      }
    } else {
      const selecteddropdown = this.elementref.nativeElement.children[1];
      this.elementref.nativeElement.focus();
      const dropdownvalue = this.elementref.nativeElement.children[0];
      const bodyrect = selecteddropdown.getBoundingClientRect();
      const activeelement = selecteddropdown.querySelectorAll('.active');
      if (selecteddropdown.classList.toggle('show')) {
       this.renderer.setStyle(selecteddropdown, 'marginTop', -25 + 'px');
      }
      if (activeelement.length !== 0) {
        this.renderer.setStyle(selecteddropdown, 'scrollTop', (<HTMLElement>activeelement[0]).offsetTop);
      }
    }
  }

  @HostListener('keydown', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    this.kcode = event.keyCode;
    if (this.kcode === 40 || this.kcode === 38 || this.kcode === 13) {
      const dropdownvalue = this.elementref.nativeElement.children[0];
      const selecteddropdown = this.elementref.nativeElement.children[1];
      let hoverelement = selecteddropdown.querySelectorAll('.dropdown-hover');
      if (hoverelement.length === 0) {
        hoverelement = selecteddropdown.querySelectorAll('.active');
      }
      if (hoverelement.length === 0) {
        hoverelement = selecteddropdown.querySelectorAll('.dropdown-option');
      }
      hoverelement[0].classList.remove('dropdown-hover');
      let menuitemtop;
      if (this.kcode === 40) {
        if (hoverelement[0].nextElementSibling === null) {
          menuitemtop = hoverelement[0].getBoundingClientRect();
          hoverelement[0].classList.add('dropdown-hover');
        } else {
          hoverelement[0].nextElementSibling.classList.add('dropdown-hover');
          const activeelement = selecteddropdown.querySelectorAll('.dropdown-hover');
          menuitemtop = hoverelement[0].nextElementSibling.getBoundingClientRect();
          (<HTMLElement>selecteddropdown).scrollTop = (<HTMLElement>activeelement[0]).offsetTop;
        }
        if (menuitemtop.top + 25 >= window.innerHeight) {
        } else {
          event.preventDefault();
          event.stopPropagation();
        }

      } else if (this.kcode === 38) {
        if (hoverelement[0].previousElementSibling === null || hoverelement[0].previousElementSibling.children.length === 0) {
          hoverelement[0].classList.add('dropdown-hover');
        } else {
          hoverelement[0].previousElementSibling.classList.add('dropdown-hover');
          const activeelement = selecteddropdown.querySelectorAll('.dropdown-hover');
          (<HTMLElement>selecteddropdown).scrollTop = (<HTMLElement>activeelement[0]).offsetTop;
        }
        event.preventDefault();
        event.stopPropagation();
      } else if (this.kcode === 13) {
        const dropdownvalue1 = hoverelement[0] as HTMLElement;
        if (selecteddropdown.classList.contains('show')) {
          dropdownvalue1.classList.add('dropdown-option-active');
          dropdownvalue1.children[0].classList.add('dropdown-option-active-a');
          this.renderer.setAttribute(selecteddropdown , 'aria-hidden', 'false');
        }
        dropdownvalue1.click();
        event.preventDefault();
        event.stopPropagation();
      }
    }
  }

  @HostListener('keyup', ['$event'])
  onKeyupHandler(event: KeyboardEvent) {
    this.kcode = event.keyCode;
    if (this.kcode === 13) {
       const selecteddropdown = this.elementref.nativeElement.children[1];
      let hoverelement = selecteddropdown.querySelectorAll('.dropdown-hover');
      if (hoverelement.length === 0) {
        hoverelement = selecteddropdown.querySelectorAll('.active');
      }
      if (hoverelement.length === 0) {}

      const dropdownvalue1 = hoverelement[0] as HTMLElement;
      dropdownvalue1.classList.remove('dropdown-option-active');
      dropdownvalue1.children[0].classList.remove('dropdown-option-active-a');
    }
    event.preventDefault();
    event.stopPropagation();
  }

  @HostListener('focus', ['$event.target'])
  public onfocus(target) {
  }
  @HostListener('blur', ['$event.target'])
  public onblur(target) {
    const Alldropdowns = this.elementref.nativeElement.querySelectorAll('.dropdown-menu');
    const selecteddropdown = this.elementref.nativeElement.children[1];
    const hoverelement = selecteddropdown.querySelectorAll('.dropdown-hover');
    if (hoverelement.length !== 0) {
      hoverelement[0].classList.remove('dropdown-hover');
    }
    for (let count = 0; count < Alldropdowns.length; count++) {
      const dropdowncontrol = Alldropdowns[count];
      if (dropdowncontrol.classList.contains('show')) {
        dropdowncontrol.classList.remove('show');
        this.renderer.setAttribute(selecteddropdown , 'aria-hidden', 'true');
      }
    }
  }
}
