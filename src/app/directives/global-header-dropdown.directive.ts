import { Directive, ElementRef, HostListener, EventEmitter, Input, Output, ViewChild, Renderer2 } from '@angular/core';

import { element } from 'protractor';
@Directive({
  selector: '[appGlobalHeaderDropdown]'
})
export class GlobalHeaderDropdownDirective {
  @Input() dropdownid: string;

  private Inside = false;
  kcode = 0;
  private accessibility = 'true';
  constructor(private _elementRef: ElementRef, private renderer: Renderer2) {
  }

  @HostListener('document:click', ['$event'])
  public outsideClick(event) {
    const selecteddropdown = this._elementRef.nativeElement.children[1];
    if (!this._elementRef.nativeElement.contains(event.target)) {
      const Alldropdowns = selecteddropdown.classList.contains('gh-dropdown-menu');
      const showDropdown = selecteddropdown.classList.contains('show');
      const hoverelement = selecteddropdown.querySelectorAll('.gh-dropdown-hover');
      if (hoverelement.length !== 0) {
        hoverelement[0].classList.remove('gh-dropdown-hover');
      }
      if (Alldropdowns && showDropdown) {
        selecteddropdown.classList.remove('show');
        this.renderer.setAttribute(selecteddropdown, 'aria-hidden', 'true');
      }
    } else {
      this._elementRef.nativeElement.focus();
      const dropdownvalue = this._elementRef.nativeElement.children[0];
      const bodyrect = selecteddropdown.getBoundingClientRect();
      const activeelement = selecteddropdown.querySelectorAll('.gh-active');
      if (selecteddropdown.classList.toggle('show')) {
        this.renderer.setStyle(selecteddropdown, 'marginTop', -(this._elementRef.nativeElement.clientHeight - 9) + 'px');
        this.renderer.setStyle(selecteddropdown, 'top', 0 + 'px');
        const hoverelement = selecteddropdown.querySelectorAll('.gh-dropdown-hover');
        if (hoverelement.length !== 0) {
          hoverelement[0].classList.remove('gh-dropdown-hover');
        }
      }
      if (activeelement.length !== 0) {
        (<HTMLElement>selecteddropdown).scrollTop = (<HTMLElement>activeelement[0]).offsetTop;
        this.accessibility === 'true' ? this.accessibility = 'false' : this.accessibility = 'true';
        this.renderer.setAttribute(selecteddropdown, 'aria-hidden', this.accessibility);
      }
    }
  }

  @HostListener('keydown', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
   // alert('key' + event.keyCode);
    this.kcode = event.keyCode;
    const selecteddropdown = this._elementRef.nativeElement.children[1];
    const showMenu = selecteddropdown.classList.contains('show');
    if (this.kcode === 40 || this.kcode === 38 || this.kcode === 13 || (this.kcode === 9 && showMenu )) {
      const dropdownvalue = this._elementRef.nativeElement.children[0];
      let hoverelement = selecteddropdown.querySelectorAll('.gh-dropdown-hover');
      if (hoverelement.length === 0) {
        hoverelement = selecteddropdown.querySelectorAll('.gh-active');
      }
      if (hoverelement.length === 0) {
        hoverelement = selecteddropdown.querySelectorAll('.gh-dropdown-option');
      }
      hoverelement[0].classList.remove('gh-dropdown-hover');
      let menuitemtop;
      if (this.kcode === 40 || (this.kcode === 9 && !event.shiftKey)) {

        if (hoverelement[0].nextElementSibling === null) {
          if (this.kcode === 9) {
            selecteddropdown.classList.remove('show');
          } else {
            hoverelement[0].classList.add('gh-dropdown-hover');
            menuitemtop = hoverelement[0].getBoundingClientRect();
          }
        } else {
          hoverelement[0].nextElementSibling.classList.add('gh-dropdown-hover');
          hoverelement[0].nextElementSibling.focus();
          if (this.kcode === 40) {
            hoverelement[0].nextElementSibling.children[0].focus();
          }
          const activeelement = selecteddropdown.querySelectorAll('.gh-dropdown-hover');
          menuitemtop = hoverelement[0].nextElementSibling.getBoundingClientRect();
          (<HTMLElement>selecteddropdown).scrollTop = (<HTMLElement>activeelement[0]).offsetTop;
        }
        if (this.kcode === 40) {
          if (menuitemtop.top + 25 >= window.innerHeight) {
          } else {
            event.preventDefault();
            event.stopPropagation();
          }
        }
      } else if (this.kcode === 38 || (this.kcode === 9 && event.shiftKey)) {
        const firstLi = hoverelement[0].previousElementSibling.classList.contains('gh-first-li');
        if (hoverelement[0].previousElementSibling === null || firstLi) {
          hoverelement[0].classList.add('gh-dropdown-hover');
        } else {
          hoverelement[0].previousElementSibling.classList.add('gh-dropdown-hover');
          hoverelement[0].previousElementSibling.children[0].focus();
          const activeelement = selecteddropdown.querySelectorAll('.gh-dropdown-hover');
          (<HTMLElement>selecteddropdown).scrollTop = (<HTMLElement>activeelement[0]).offsetTop;
        }
        event.preventDefault();
        event.stopPropagation();
      } else if (this.kcode === 13) {
        const dropdownvalue1 = hoverelement[0] as HTMLElement;
        if (selecteddropdown.classList.contains('show')) {
          this.renderer.setAttribute(selecteddropdown, 'aria-hidden', 'false');
        }
        dropdownvalue1.click();
        event.preventDefault();
        event.stopPropagation();

      }
    }
  }

}
