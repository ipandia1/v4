import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appCheckBoxDirective]'
})
export class CheckBoxDirective {
  constructor(private el: ElementRef) { }
  @HostListener('keypress', ['$event', '$event.currentTarget'])
  onkeypress(event: KeyboardEvent, element) {
    if (event.keyCode === 13) {
      if (element.classList.contains('indicator')) {
        const checkboxSelected = this.el.nativeElement.previousElementSibling as HTMLInputElement;
        if (!checkboxSelected.disabled) {
          checkboxSelected.click();
        }
      }
    }
  }
}
