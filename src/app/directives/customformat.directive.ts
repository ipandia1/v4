import { Directive, ElementRef, HostListener, Output, EventEmitter, Input, ChangeDetectorRef } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appCustomformat]',
})
export class CustomformatDirective {
  @Input() format: string;
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  str: string;
  arr: any = [];
  zeroposition: string;
  constructor(private el: ElementRef, private control: NgControl) { }
  @HostListener('keydown', ['$event']) onkeydown(e): boolean {
    if (e.keyCode === 8 || (e.which >= 48 && e.which <= 57) ||
      (e.which >= 96 && e.which <= 105) || [8, 9, 13, 27, 37, 38, 39, 40].indexOf(e.which) > -1) {
      return true;
    } else {
      e.preventDefault();
      return false;
    }
  }
  @HostListener('keyup', ['$event']) onkeyup(e): boolean {
    if (e.which === 8 || [8, 9, 13, 27, 37, 38, 39, 40].indexOf(e.which) > -1) {
      return true;
    }
    const maxlength = this.el.nativeElement.getAttribute('maxlength');
    const prev = this.el.nativeElement.value;
    let cursorposition = this.el.nativeElement.selectionStart;
    if (e.target.value.length > maxlength) {
      e.target.value = e.target.value.substring(0, maxlength);
    }

    this.str = e.target.value + '';
    this.arr = this.str.split('');
    const pos = e.target.value.indexOf('/');
    if (e.target.value.split('.').length === 2) {
      return false;
    }
    e.target.value = this.arr.filter(c => isFinite(c)).join('');
    // tslint:disable-next-line:radix
    if (isNaN(parseInt(e.target.value))) {
      e.target.value = '';
      this.ngModelChange.emit((e.target.value === '') ? null : e.target.value);
    }

    if (e.target.value !== '' && this.format === 'expirydate') {
      e.target.value = e.target.value.replace(
        /^([1-9]\/|[2-9])$/g, '0$1/'
      ).replace(
        /^(0[1-9]|1[0-2])$/g, '$1/'
        ).replace(
        /^([0-1])([3-9])$/g, '0$1/$2'
        ).replace(
        /^([0]+)\/|[0]+$/g, '0'
        ).replace(
        /[^\d\/]|^[\/]*$/g, ''
        ).replace(
        /\/\//g, '/'
        );

      if (prev + '/' !== e.target.value) {
        if (e.target.value.indexOf('/') === -1 && e.target.value.length > 1) {
          e.target.value = e.target.value.substring(0, 2) + '/' + e.target.value.substring(2, 4);
        }
      } else {
        e.target.value = prev;
      }
    } else if (e.target.value !== '' && this.format === 'routingNumber') {
      e.target.value = e.target.value.replace(/[^\d\/]|^[\/]*$/g, '');
      e.target.value = e.target.value.replace(/(\d{3})/g, '$1 ').replace(/(^\s+|\s+$)/, '');
      const htmele = this.el.nativeElement as HTMLInputElement;
      if (htmele.setSelectionRange) {
        htmele.focus();
        cursorposition = cursorposition + (this.el.nativeElement.value.length - prev.length);
        setTimeout(() => {
          htmele.setSelectionRange(cursorposition, cursorposition);
        }, 0);
      }
      if (e.target.value.length > maxlength) {
        e.target.value = e.target.value.substring(0, maxlength);
      }
    }else if (e.target.value !== '' && this.format === 'numbersOnly') {
      e.target.value = e.target.value.replace(/[^\d\/]|^[\/]*$/g, '');
      if (e.target.value.length > maxlength) {
        e.target.value = e.target.value.substring(0, maxlength);
      }
    }else if (e.target.value !== '' && this.format === 'creditCardNumber') {
      e.target.value = e.target.value.replace(/[^\d\/]|^[\/]*$/g, '');

      e.target.value = e.target.value.replace(/(\d{4})/g, '$1 ').replace(/(^\s+|\s+$)/, '');
      const htmele = this.el.nativeElement as HTMLInputElement;

      if (htmele.setSelectionRange) {
        htmele.focus();
        cursorposition = cursorposition + (this.el.nativeElement.value.length - prev.length);
        setTimeout(() => {
          htmele.setSelectionRange(cursorposition, cursorposition);
        }, 0);
      }
      if (e.target.value.length > maxlength) {
        e.target.value = e.target.value.substring(0, maxlength);
      }
    } else if (e.target.value !== '' && this.format === 'phonenumber') {
      e.target.value = e.target.value.replace(/[^\d\/]|^[\/]*$/g, '');

      const a = e.target.value.match(/\d*/g).join('')
        .match(/(\d{0,3})(\d{0,3})(\d{0,4})/).slice(1);
      e.target.value = ['(', a[0], ') ', a[1], '-', a[2]].join('');
      e.target.value = e.target.value.replace(') -', '');
      if (e.target.value.lastIndexOf('-') === e.target.value.length - 1) {
        e.target.value = e.target.value.slice(0, e.target.value.length - 1);
      }
      if (e.target.value.length > maxlength) {
        e.target.value = e.target.value.substring(0, maxlength);
      }
    } else if (e.target.value !== '' && this.format === 'currency') {
      e.target.value = e.target.value.replace(/[^\d\/]|^[\/]*$/g, '');
      const num = e.target.value;
      e.target.value = '$' + Number.parseFloat(num).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    } else if (e.target.value !== '' && this.format === 'creditCardNumberFormat') {
      e.target.value = e.target.value.replace(/[^\d\/]|^[\/]*$/g, '');
      e.target.value = e.target.value.replace(/(\d{4})/g, '$1-').replace(/(^\s+|\s+$)/, '');
      const htmele = this.el.nativeElement as HTMLInputElement;
      if (htmele.setSelectionRange) {
        htmele.focus();
        cursorposition = cursorposition + (this.el.nativeElement.value.length - prev.length);
        setTimeout(() => {
          htmele.setSelectionRange(cursorposition, cursorposition);
        }, 0);
      }
      if (e.target.value.length > maxlength) {
        e.target.value = e.target.value.substring(0, maxlength);
      }
    } else {
      e.target.value = e.target.value.replace(/[^\d\/]|^[\/]*$/g, '');
    }
    this.ngModelChange.emit((e.target.value === '') ? null : e.target.value);
    return true;
  }
}
