import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { posix } from 'path';

@Directive({
  selector: '[appButtonripple]'
})
export class ButtonrippleDirective {

  constructor(private elementref: ElementRef, private renderer: Renderer2) {}

  @HostListener('click', ['$event', '$event.currentTarget'])
  click(event, element) {
    if (element.querySelector('.ripple-animate') == null) {
      // tslint:disable-next-line:prefer-const
      let ripple = this.renderer.createElement('span');
      const currentelementpositions = element.getBoundingClientRect();
      let radius;
      if (currentelementpositions.height > currentelementpositions.width) {
        radius = currentelementpositions.height;
      } else {
        radius = currentelementpositions.width;
      }
      const scrolltop = (document.documentElement &&
      document.documentElement.scrollTop) || document.body.scrollTop;
      const scrollleft = (document.documentElement && document.documentElement.scrollLeft) || document.body.scrollLeft;
      const rippleleft = event.pageX - currentelementpositions.left - radius / 2 - scrollleft;
      const rippletop = event.pageY - currentelementpositions.top - radius / 2 - scrolltop;
      ripple.className = 'ripple-animate';
      ripple.style.width = ripple.style.height = radius + 'px';
      ripple.style.left = rippleleft + 'px';
      ripple.style.top = rippletop + 'px';
      ripple.addEventListener('animationend', () => {
        element.removeChild(ripple);
      });
      element.appendChild(ripple);
    }
  }

}
