import { Component, OnInit, ViewEncapsulation, EventEmitter } from '@angular/core';
import { fail } from 'assert';
import { Router } from '@angular/router';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-home-root',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  selectedVal = 'Select';
  showStyleGuide = false;
  showStyleGuideAlerts = false;
  showFamilyAllowance = false;
  showTmoHome = true;
  showErrorHandling = true;
  showPortIn = true;
  showStandalone = true;
  refill = true;
  selecthome = 'homeid';
  pageNames: Array<any> = [
    { name: 'Select', disabled: true },
    // { name: 'Style Guide', disabled: false },
    { name: 'Family Allowance', disabled: false },
    { name: 'TMO Home', disabled: false },
    { name : 'Error Handling', disabled: false},
    { name : 'Port In', disabled: false},
    { name : 'Schduled calls', disabled: false},
    { name : 'Refill', disabled: false}
  ];

  callBackValue = new EventEmitter<string>();
  constructor(private commonService: CommonService) {

  }

  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedVal = callBackValue; // can use selectedVal variable whenever we want to use the selected value
    }
  }
  ngOnInit() {
    const selectitem = localStorage.getItem('selectedval');
    if (selectitem != null) {
      this.selectedValue(selectitem);
    } else {
      this.selectedValue(this.selectedVal);
    }

    this.commonService.showSpinner(true, true, 1000);

  }
  selectedValue(value: string): any {
    this.selectedVal = value;
    this.callBackValue.emit(this.selectedVal);
    if (this.selectedVal === 'Style Guide') { this.showStyleGuide = true; } else { this.showStyleGuide = false; }
    if (this.selectedVal === 'Family Allowance') { this.showFamilyAllowance = true; } else { this.showFamilyAllowance = false; }
    if (this.selectedVal === 'TMO Home') { this.showTmoHome = true; } else { this.showTmoHome = false; }
    if (this.selectedVal === 'Error Handling') { this.showErrorHandling = true; } else { this.showErrorHandling = false; }
    if (this.selectedVal === 'Port In') { this.showPortIn = true; } else { this.showPortIn = false; }
    if (this.selectedVal === 'Schduled calls') { this.showStandalone = true; } else { this.showStandalone = false; }
    if (this.selectedVal === 'Refill') { this.refill = true; } else { this.refill = false; }
    localStorage.setItem('selectedval', value);
  }

}
