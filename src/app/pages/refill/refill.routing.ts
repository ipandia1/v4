import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RefillAccountComponent } from './refill-account/refill-account.component';
import { RefillInputPaymentComponent } from './refill-input-payment/refill-input-payment.component';
import { RefillInputComponent } from './refill-input/refill-input.component';
import { RefillConfirmationComponent } from './refill-confirmation/refill-confirmation.component';

const routes: Routes = [
  { path: 'refillaccount', component: RefillAccountComponent },
  { path: 'refillinput', component: RefillInputComponent },
  { path: 'refillinputpayment', component: RefillInputPaymentComponent },
  { path: 'Refillconfirmation', component: RefillConfirmationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RefillRouting { }
