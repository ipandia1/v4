import { Component, OnInit, Renderer2, HostListener } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-refill-account',
  templateUrl: './refill-account.component.html',
  styleUrls: ['./refill-account.component.scss']
})
export class RefillAccountComponent implements OnInit {
  jsonPath = 'assets/json/refill/refill-account.json';
  accountData: any;
  isDataLoaded: boolean;
  showNotification: boolean;
  onState = true;
  constructor(private commonService: CommonService, private renderer: Renderer2) {
  }

  ngOnInit() {
    this.commonService.visibleHeader = false;
    this.commonService.getJsonData(this.jsonPath)
      .subscribe(responseJson => {
        this.accountData = responseJson;
        this.isDataLoaded = true;
      });
    this.commonService.showGlobalHeader();
    this.commonService.showGlobalFooter();
    this.commonService.showTmoHeader();
  }
  @HostListener('click', ['$event']) onClick(e) {
    if (!e.target.classList.contains('notification-down')) {
      if (this.showNotification) {
        this.showNotification = false;
      }
    }
    else {
      this.showNotification = !this.showNotification;
    }
  }
  @HostListener('keyup', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    const sourceElement = event.srcElement;
    const notificationsSection = this.renderer.selectRootElement('#notifications-section', true);
    const checkElement = notificationsSection.contains(sourceElement);
    if (!checkElement) {
      this.showNotification = false;
    }
  }
  closeNotification() {
    this.showNotification = false;
  }
  eventHandler(event) {
    if (event.keyCode === 13) {
      this.showNotification = !this.showNotification;
    }
  }
  toggle() {
    const btnState = this.renderer.selectRootElement('.slider-btn', true);
    if (btnState.checked) {
      btnState.checked = false;
    } else {
      btnState.checked = true;
    }

    this.clickFun();
  }
  clickFun() {
    this.onState = !this.onState;
  }
}
