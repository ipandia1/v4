import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-refill-input-payment',
  templateUrl: './refill-input-payment.component.html',
  styleUrls: ['./refill-input-payment.component.scss']
})
export class RefillInputPaymentComponent implements OnInit {
  refillJsonPath = 'assets/json/refill/refill-account.json';
  refillData: any;
  isDataLoaded: boolean;

  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.commonService.getJsonData(this.refillJsonPath)
      .subscribe(responseJson => {
        this.refillData = responseJson;
        this.isDataLoaded = true;
      });
    }
}
