import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-refill-input',
  templateUrl: './refill-input.component.html',
  styleUrls: ['./refill-input.component.scss']
})
export class RefillInputComponent implements OnInit {
  refillJsonPath = 'assets/json/refill/refill-account.json';
  refillData: any;
  isDataLoaded: boolean;
  pinNumber: string;

  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.commonService.getJsonData(this.refillJsonPath)
    .subscribe(responseJson => {
      this.refillData = responseJson;
      this.isDataLoaded = true;
    });
  }


}
