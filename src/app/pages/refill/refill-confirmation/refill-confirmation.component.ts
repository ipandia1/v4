import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-refill-confirmation',
  templateUrl: './refill-confirmation.component.html',
  styleUrls: ['./refill-confirmation.component.scss']
})
export class RefillConfirmationComponent implements OnInit {
  oderDetails = [
    { descTitle: 'Order confirmation', value: '$50.00' },
    { descTitle: 'Added to your account', value: '$50.00' },
    { descTitle: 'New account balance', value: '$130.00' },
    { descTitle: 'Card Value reedeemed', value: '$50.00' },
    { descTitle: 'Used by', value: '$0.00' }
  ];

  accountDetails = [
    { descTitle: 'Account summary ', value: '' },
    { descTitle: 'Payment date', value: '08/03/2019' },
    { descTitle: 'Payment amount', value: '$50.00' },
    { descTitle: 'Account balance', value: '$130.00  (use by 11/13/2019)' },
    { descTitle: 'Minimum refill amount to renew plan', value: '$0.00' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
