import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RefillRouting } from './refill.routing';
import { RefillAccountComponent } from './refill-account/refill-account.component';
import { RefillInputPaymentComponent } from './refill-input-payment/refill-input-payment.component';
import { RefillInputComponent } from './refill-input/refill-input.component';
import { RefillConfirmationComponent } from './refill-confirmation/refill-confirmation.component';
import { FormsModule } from '@angular/forms';
import { ReusableModule } from '../../ui-components/reusable/reusable.module';
@NgModule({
  imports: [
    CommonModule, RefillRouting, FormsModule, ReusableModule
  ],
  declarations: [ RefillAccountComponent,
    RefillInputPaymentComponent,
    RefillInputComponent,
    RefillConfirmationComponent]
})
export class RefillModule { }
