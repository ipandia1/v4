import { Component, OnInit, HostListener, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-schduled-call',
  templateUrl: './schduled_call.component.html',
  styleUrls: ['./schduled_call.component.scss']
})
export class SchduledCallComponent implements OnInit {
  showDetails = true;
  isDesktop: boolean;
  isExtraSmallScreen: boolean;
  mobile: boolean;
  activeIndexItem = 0;
  cancelButtonText: string;
  lastVal: any = -1;
  visible = false;
  inputtextfield: InputTextField;
  callBackVisible = false;
  confrmnVisible = false;
  scheduleConfrmnVisible = false;
  selecteddate = 0;
  number = '';
  selectedLine1 = 'Mon 00, 0000';
  selectedLine2 = '00:00  AM';
  selectedLine3 = 'Pacific';
  dateDetails = [{ 'name': 'Mon 00, 0000' }, { 'name': 'Tue 00, 0000' }, { 'name': 'Wed 00, 0000' }];
  timeDetails = [{ 'name': '00:00  AM' }, { 'name': '01:00  AM' }, { 'name': '02:00  AM' }];
  timeZone = [{ 'name': 'Pacific' }, { 'name': 'India' }, { 'name': 'Eastern' }];
  dateTimeDetails = [{ 'time': '00:00 AM', 'date': 'MON 00' }, { 'time': '00:00 AM', 'date': 'MON 00' },{ 'time': '00:00 AM', 'date': 'MON 00' }, { 'time': '00:00 AM', 'date': 'MON 00' }];
  body = document.getElementsByTagName('body')[0];
  dropdownText = '';

  selectedVal = 'select';
  selectedRange = 'SEP 27TH - OCT 26TH';
  selectline = 'selectline';
  callBackValue = new EventEmitter<string>();
  datePeriod = [
    { name: 'JAN 27TH - FEB 26TH', disabled: true },
    { name: 'FEB 27TH - MAR 26TH', disabled: true },
    { name: 'MAR 27TH - APR 26TH', disabled: true },
    { name: 'APR 27TH - MAY 26TH', disabled: true },
    { name: 'MAY 27TH - JUN 26TH', disabled: true },
    { name: 'JUN 27TH - JUL 26TH', disabled: true },
    { name: 'JUL 27TH - AUG 26TH', disabled: true },
    { name: 'AUG 27TH - SEP 26TH', disabled: false },
    { name: 'SEP 27TH - OCT 26TH', disabled: false },
    { name: 'OCT 27TH - NOV 26TH', disabled: false },
    { name: 'NOV 27TH - DEC 26TH', disabled: false },
  ];

  selectedValue(value: string): any {
    this.selectedVal = value;
    this.callBackValue.emit(this.selectedVal);
  }

  constructor(private router: Router) { }

  ngOnInit() {
    if (window.innerWidth > 767) {
      this.isDesktop = true;
      this.isExtraSmallScreen = false;
      this.cancelButtonText = 'Cancel callback';
    } else if (window.innerWidth > 374 && window.innerWidth < 768 ) {
      this.isDesktop = false;
      this.isExtraSmallScreen = true;
      this.cancelButtonText = 'Cancel';
    } else {
      this.isExtraSmallScreen = true;
      this.cancelButtonText = 'Cancel';
    }
    this.inputtextfield = {
      inputText: ''
    };
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth > 767) {
      this.isDesktop = true;
      this.isExtraSmallScreen = false;
      this.cancelButtonText = 'Cancel callback';
    } else if (window.innerWidth > 374 && window.innerWidth < 768 ) {
      this.isDesktop = false;
      this.isExtraSmallScreen = true;
      this.cancelButtonText = 'Cancel';
    } else {
      this.isDesktop = false;
      this.isExtraSmallScreen = true;
      this.cancelButtonText = 'Cancel';
    }
  }
  showFunc = function (val) {
    if (this.showDetails) {
      this.hideVar = val;
      if (this.lastVal === this.hideVar) {
        this.hideVar = -1;
      }
      this.lastVal = this.hideVar;
    }
  };

  // esc key starts

@HostListener('window:keydown', ['$event'])
onKeyDownHandler(event: KeyboardEvent) {
  // console.log('hi', event.key);
  if (event.key === 'Escape' || event.key === 'Esc') {
    this.close();
  }
}
  // esc key ends

  // call back starts
  showScroll() {
    this.body.classList.remove('gm-noScroll');
  }
  hideScroll() {
    this.body.classList.add('gm-noScroll');
  }
  close() {
    this.visible = false;
    this.callBackVisible = false;
    this.confrmnVisible = false;
    this.scheduleConfrmnVisible = false;
    this.showScroll();
    this.inputtextfield = {
      inputText: ''
    };
    this.number = '';
    this.selectedLine1 = 'Mon 00, 0000';
    this.selectedLine2 = '00:00  AM';
    this.selectedLine3 = 'Pacific';
  }
  select(value, type) {
    if (value) {
      if (type === 'date') {
        this.selectedLine1 = value;
      }
      if (type === 'time') {
        this.selectedLine2 = value;
      }
      if (type === 'zone') {
        this.selectedLine3 = value;
      }
    }
  }
  // closeCallBack() {
  //   this.callBackVisible = false;
  //   this.body.classList.remove('noScroll');
  // }
  change() {
    this.hideScroll();
    this.visible = !this.visible;
    document.getElementById('callUS').focus();
    // this.modelTab('callUS');
  }
  scheduleCall() {
    this.visible = false;
    this.scheduleConfrmnVisible = false;
    this.callBackVisible = true;
    document.getElementById('scheduleCallContainer').focus();
    // this.modelTab('scheduleCallContainer');
  }
  confirm() {
    this.hideScroll();
    this.callBackVisible = false;
    this.confrmnVisible = true;
    document.getElementById('confirmationContainer').focus();
    // this.modelTab('confirmationContainer');
  }
  done() {
    this.hideScroll();
    this.confrmnVisible = false;
    this.scheduleConfrmnVisible = true;
    document.getElementById('confirmScheduleContainer').focus();
    // this.modelTab('confirmScheduleContainer');
  }
  // modelTab(event, divID) {
  //   event.preventDefault();
  //   const dialog = document.getElementById(divID);
  //   dialog.focus();
  // }

  selectdatetime(indexval) {
  this.selecteddate = indexval;
  }
  onKeyDown = function(event, index) {
    this.mobile = false;
    if (window.innerWidth < 768) {
        this.mobile = true;
    }
   if ((event.keyCode === 37 && !this.mobile) || (event.keyCode === 38 && !this.mobile)) {
       if (this.activeIndexItem === 0) {
           this.activeIndexItem = this.dateTimeDetails.length - 1;
       } else {
           this.activeIndexItem -= 1;
       }
       document.getElementById('dateButton' + this.activeIndexItem).focus();

   }
   if ((event.keyCode === 39 && !this.mobile) || (event.keyCode === 40 && !this.mobile)) {
       if (this.activeIndexItem === this.dateTimeDetails.length - 1) {
           this.activeIndexItem = 0;
       } else {
            this.activeIndexItem += 1;
       }
       document.getElementById('dateButton' + this.activeIndexItem).focus();
   }
  }




  backTabEvent(event, mainId, lastId) {
  const allElements = document.getElementById(mainId).querySelectorAll
  ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
    if (document.activeElement === allElements[0]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          setTimeout(function() {
            document.getElementById(lastId).focus();
          }, 0);
        }
      }
    }
    if (document.activeElement === document.getElementById(mainId)) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          setTimeout(function() {
            document.getElementById(lastId).focus();
          }, 0);
        }
      }
    }
    if (document.activeElement === allElements[allElements.length - 1]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {}
        else {
        event.preventDefault();
        setTimeout(function() {
          document.getElementById(mainId).focus();
        }, 0);
      }

    }
    }
}
}

export class InputTextField {
  inputText: string;
}
