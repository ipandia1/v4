import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-systemerror',
  templateUrl: './systemerror.component.html',
  styleUrls: ['./systemerror.component.scss']
})
export class SystemerrorComponent implements OnInit {
  smallDevice = false;
  Errorname = 'Oops! Something went wrong.';
  Errormessage = 'We ran into a technical problem while processing your payment. Please try again, or message us for help with this transaction.';
  showDetails = false;
  constructor() { }
  ngOnInit() {
    const deviceHeight = window.innerHeight;
    if (deviceHeight < 565) {
      this.smallDevice = true;
    } else {
      this.smallDevice = false;
    }
  }

  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  };

}
