import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-errorstate-modal',
  templateUrl: './errorstate-modal.component.html',
  styleUrls: ['./errorstate-modal.component.scss']
})
export class ErrorstateModalComponent {
  visible: boolean;
  constructor(private renderer: Renderer2) { }
    close() {
      this.visible = false;
    }
  focusIn() {
    const dialogFocus = this.renderer.selectRootElement('#modalBody', true);
    dialogFocus.focus();
  }
}
