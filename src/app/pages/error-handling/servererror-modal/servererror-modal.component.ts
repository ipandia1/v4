import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-servererror-modal',
  templateUrl: './servererror-modal.component.html',
  styleUrls: ['./servererror-modal.component.scss']
})
export class ServererrorModalComponent {
  visible: boolean;
  constructor( private renderer: Renderer2) { }
  close() {
    this.visible = false;
  }
  focusIn() {
    const dialogFocus = this.renderer.selectRootElement('#modalBody', true);
    dialogFocus.focus();
  }
}
