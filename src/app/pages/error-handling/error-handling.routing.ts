import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorstateModalComponent } from './errorstate-modal/errorstate-modal.component';
import { ServererrorModalComponent } from './servererror-modal/servererror-modal.component';
import { SystemerrorComponent } from './systemerror/systemerror.component';

const routes: Routes = [
  { path: 'errorstate-modal', component: ErrorstateModalComponent },
  { path: 'servererror-modal', component: ServererrorModalComponent },
  { path: 'systemerror', component: SystemerrorComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorHandlingRoutingModule { }
