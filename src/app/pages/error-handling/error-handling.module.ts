import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorHandlingRoutingModule } from './error-handling.routing';
import { ErrorstateModalComponent } from './errorstate-modal/errorstate-modal.component';
import { ServererrorModalComponent } from './servererror-modal/servererror-modal.component';
import { SystemerrorComponent } from './systemerror/systemerror.component';
// tslint:disable-next-line:max-line-length
import { ReusableModule } from '../../ui-components/reusable/reusable.module';

@NgModule({
  imports: [
    CommonModule, ErrorHandlingRoutingModule, ReusableModule, FormsModule, ReactiveFormsModule
  ],
  declarations: [
    ErrorstateModalComponent,
    ServererrorModalComponent,
    SystemerrorComponent]
})
export class ErrorHandlingModule { }
