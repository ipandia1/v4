import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-problem-fix',
  templateUrl: './problem-fix.component.html',
  styleUrls: ['./problem-fix.component.scss']
})
export class ProblemFixComponent implements OnInit {
  details = {
    'title': 'Thanks for the help!',
    'content': 'Looks like this issue has been resolved. We’ll let you know when your phone number has been transferred to T-Mobile.'
  };
  constructor() { }

  ngOnInit() {
  }
}
