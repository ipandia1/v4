import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success-status',
  templateUrl: './success-status.component.html',
  styleUrls: ['./success-status.component.scss']
})
export class SuccessStatusComponent implements OnInit {

  detailsArray = [{
    'title': 'Success headline goes here',
    // tslint:disable-next-line:max-line-length
    'content': 'Success copy goes here. Success copy goes here. Success copy goes here. Success copy goes here. Success copy goes here. Success copy goes here. Success copy goes here. Success copy goes here. Success copy goes here. Success copy goes here. Success copy goes here.'
  }];
  successFromResubmit = true; // make it false if you want see the error fix status template page
  constructor() { }

  ngOnInit() {
  }
}
