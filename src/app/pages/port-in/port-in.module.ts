import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortInRouting } from './port-in.routing';
import { MaximumAttemptsComponent } from '../port-in/maximum-attempts/maximum-attempts.component';
import { ProblemFixComponent } from '../port-in/problem-fix/problem-fix.component';
import { ProblemIdentifierComponent } from '../port-in/problem-identifier/problem-identifier.component';
import { SuccessStatusComponent } from '../port-in/success-status/success-status.component';
import { ReusableModule } from '../../ui-components/reusable/reusable.module';

@NgModule({
  imports: [
    CommonModule, PortInRouting, ReusableModule
  ],
  declarations: [MaximumAttemptsComponent, ProblemFixComponent, ProblemIdentifierComponent, SuccessStatusComponent]
})
export class PortInModule { }
