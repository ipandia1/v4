import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-problem-identifier',
  templateUrl: './problem-identifier.component.html',
  styleUrls: ['./problem-identifier.component.scss']
})
export class ProblemIdentifierComponent implements OnInit {
  errorMessageValid: string;
  errorMessage: string;
  minlength: string;
  maxl: string;
  inputType = {
    accountNumber: '',
    zipcode: '',
    pincode: '',
    showPincode : true,
    showZipcode : true,
    showAccountno : true
  };
   constructor() { }
    ngOnInit() {
    }
}
