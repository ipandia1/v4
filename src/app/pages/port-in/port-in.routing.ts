import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaximumAttemptsComponent } from '../port-in/maximum-attempts/maximum-attempts.component';
import { ProblemFixComponent } from '../port-in/problem-fix/problem-fix.component';
import { ProblemIdentifierComponent } from '../port-in/problem-identifier/problem-identifier.component';
import { SuccessStatusComponent } from '../port-in/success-status/success-status.component';

const routes: Routes = [
  { path: 'maximumAttempts', component: MaximumAttemptsComponent },
  { path: 'problemFix', component: ProblemFixComponent },
  { path: 'problemIdentifier', component: ProblemIdentifierComponent },
  { path: 'successStatus', component: SuccessStatusComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortInRouting { }
