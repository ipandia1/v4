import { Component, OnInit, HostListener, Renderer2 } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './tmo-home-page.component.html',
  styleUrls: ['./tmo-home-page.component.scss']
})
export class TmoHomePageComponent implements OnInit {
  constructor(private commonService: CommonService, private renderer: Renderer2) {
  }

  // homePageJsonPath = 'assets/json/tmo-home/home.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-no-notification.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-autopay.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-nonPah-upgrade.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-nonPah-noupgrade.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-no-payment.json';
   homePageJsonPath = 'assets/json/tmo-home/home-alert.json';

  homePageData: any;
  showNotification: boolean;
  showDisclaimer: boolean;
  isDataLoaded = false;
  isEmptyObject = '{}';
  isPaymentDue = true;
  isPaymentDueCount: number;
  dropdownidval = 'test';
  pencilImg = 'assets/img/png/pencilImg.PNG';
  editName: any = {
    firstName: '',
    lastName: ''
  };
  phonenumber:string;
  // notification
  showAlert = true;
  tooltipText = "Tooltip text lorem ipsum dolor sit.";
  visible: boolean;
  @HostListener('click', ['$event']) onClick(e) {
    if (!e.target.classList.contains('notification-down')) {
      if (this.showNotification) {
        this.showNotification = false;
      }
    }
    else {
     this.showNotification = !this.showNotification;
     }
  }
  @HostListener('keyup', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    const sourceElement = event.srcElement;
    const notificationsSection = this.renderer.selectRootElement('#notifications-section', true);
    const checkElement = notificationsSection.contains(sourceElement);
    if (!checkElement) {
      this.showNotification = false;
    }
  }
  ngOnInit() {
    // change to true/false to hide/show the discalimer in the t-mobile benefits section
    this.showDisclaimer = true;
    this.commonService.visibleHeader = false;
    this.commonService.getJsonData(this.homePageJsonPath)
      .subscribe(responseJson => {
        this.homePageData = responseJson;
        this.isDataLoaded = true;
        this.isPaymentDueCount = this.calculatingPaymentDueday();
      });
    this.commonService.showGlobalHeader();
    this.commonService.showGlobalFooter();
    this.commonService.showTmoHeader();
  }
  // calculating payment due date from current date
  calculatingPaymentDueday() {
    const d1 = new Date();
    const date1 = (d1.getMonth() + 1) + '/' + d1.getDate() + '/' + d1.getFullYear();
    const d2 = new Date(this.homePageData['paymentDetails'][0].paymentDueDate);
    const date2 = (d2.getMonth() + 1) + '/' + d2.getDate() + '/' + d2.getFullYear();
    function parseDate(str) {
      const mdy = str.split('/');
      return new Date(mdy[2], mdy[0] - 1, mdy[1]);
    }
    function datediff(first, second) {
      return Math.round((first - second) / (1000 * 60 * 60 * 24));
    }
    const count = datediff(parseDate(date1), parseDate(date2));
    if (count >= 4 && count <= 20) {
      this.isPaymentDue = true;
      return count;
    }
  }
  openNotification() {
    //this.showNotification = true;
  }
  closeNotification() {
    this.showNotification = false;
  }
  closeAlert(showAlert) {
    this.showAlert = showAlert;
    if (showAlert.keyCode === 13) {
      this.showAlert = !showAlert;
    }
  }
  eventHandler(event) {
    if (event.keyCode === 13) {
      this.showNotification = !this.showNotification;
    }
  }
  description(data) {
    return data.replace(/-/gi, ' ');
  }

  close() {
    console.log('close');
    this.visible = false;
    this.commonService.showScroll(true);
    const firstnameele = this.renderer.selectRootElement('#name', true);
  setTimeout(() =>
  firstnameele.focus(),
   0);
  }
focusIn(event) {
  event.preventDefault();
  event.stopPropagation();
  const dialogFocus = this.renderer.selectRootElement('#firstName', true);
  console.log('focus');
  setTimeout(() =>{
    this.commonService.showScroll(true);
  dialogFocus.focus()},
   0);
}

openEditModal() {
  this.visible = true;
  var fullName = this.homePageData.accountName;
  this.phonenumber = this.homePageData.accountId;
  var index = fullName.indexOf(' ');
  if (index >= 0) {
    var fname = this.homePageData.accountName.substr(0, index); 
    var lname = this.homePageData.accountName.substr(index + 1);
  } else {
   fname = fullName;
   lname = '';
  }
  this.editName = {
    firstName: fname,
    lastName: lname
  };

  this.homePageData.accountName = this.editName.firstName + ' ' + this.editName.lastName;
  const firstnameele = this.renderer.selectRootElement('#firstName', true);
  setTimeout(() =>
  firstnameele.focus(),
   0);
   this.commonService.showScroll(false);
}
save() {
  this.visible = false;
  this.homePageData.accountName = this.editName.firstName + ' ' + this.editName.lastName;
  this.commonService.showScroll(true);
  const firstnameele = this.renderer.selectRootElement('#name', true);
  setTimeout(() =>
  firstnameele.focus(),
   0);
}
}
