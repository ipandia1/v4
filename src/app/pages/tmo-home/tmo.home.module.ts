import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TmoHomePageComponent } from './tmo-home-page/tmo-home-page.component';
import { TmoHomeRoutingModule } from './tmo.home.routing';
import { ReusableModule } from '../../ui-components/reusable/reusable.module';
import { TmoHomeTopBannerPageComponent } from './tmo-home-top-banner-page/tmo-home-top-banner-page.component';
import { TmoHomePrepaidComponent } from './tmo-home-page-prepaid/tmo-home-page-prepaid.component';

@NgModule({
  imports: [
    CommonModule, TmoHomeRoutingModule, ReusableModule
  ],
  declarations: [TmoHomePageComponent, TmoHomeTopBannerPageComponent, TmoHomePrepaidComponent]
})
export class TmoModule { }
