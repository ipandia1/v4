import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TmoHomePageComponent } from './tmo-home-page/tmo-home-page.component';
import { TmoHomeTopBannerPageComponent } from './tmo-home-top-banner-page/tmo-home-top-banner-page.component';
import { TmoHomePrepaidComponent } from './tmo-home-page-prepaid/tmo-home-page-prepaid.component';

const routes: Routes = [
  { path: 'tmo-home-page', component: TmoHomePageComponent },
  { path: 'tmo-home-top-banner-page', component: TmoHomeTopBannerPageComponent
  },
  { path: 'tmo-home-prepaid-page', component: TmoHomePrepaidComponent }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TmoHomeRoutingModule { }
