import { Component, OnInit, HostListener, Renderer2 } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tmo-home-prepaid',
  templateUrl: './tmo-home-page-prepaid.component.html',
  styleUrls: ['./tmo-home-page-prepaid.component.scss']
})
export class TmoHomePrepaidComponent implements OnInit {

   //homePageJsonPath = 'assets/json/tmo-home/home-prepaid-as-you-go.json';
  homePageJsonPath = 'assets/json/tmo-home/home-prepaid-low-funds.json';
   //homePageJsonPath = 'assets/json/tmo-home/home-prepaid-active-unlimited.json';
  //homePageJsonPath = 'assets/json/tmo-home/home-prepaid-autopay.json';


  bingeOn = true;
  homePageData: any;
  showNotification: boolean;
  showDisclaimer: boolean;
  isDataLoaded = false;
  onState = true;
  isEmptyObject = '{}';
  isPaymentDue = true;
  // notification
  showAlert = false;
  tooltipText = 'Tooltip text lorem ipsum dolor sit.';
  constructor(private commonService: CommonService, private renderer: Renderer2) {
  }
  @HostListener('click', ['$event']) onClick(e) {
   // console.log(e.target);
    if (!e.target.classList.contains('notification-down')) {
      if (this.showNotification) {
        this.showNotification = false;
      }
    } else {
      this.showNotification = !this.showNotification;
    }
  }
  @HostListener('keyup', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    const sourceElement = event.srcElement;
    const notificationsSection = this.renderer.selectRootElement('#notifications-section', true);
    const checkElement = notificationsSection.contains(sourceElement);
    if (!checkElement) {
      this.showNotification = false;
    }
  }
  ngOnInit() {
    // change to true/false to hide/show the discalimer in the t-mobile benefits section
    this.showDisclaimer = true;
    this.commonService.visibleHeader = false;
    this.commonService.getJsonData(this.homePageJsonPath)
      .subscribe(responseJson => {
        this.homePageData = responseJson;
     //   console.log(this.homePageData.accountBalance[0].accountInfo.status)
        this.isDataLoaded = true;

      });
    this.commonService.showGlobalHeader();
    this.commonService.showGlobalFooter();
    this.commonService.showTmoHeader();
  }
  // calculating payment due date from current date

  openNotification() {
    // this.showNotification = true;
  }
  closeNotification() {
    this.showNotification = false;
  }
  closeAlert(showAlert) {
    this.showAlert = showAlert;
    if (showAlert.keyCode === 13) {
      this.showAlert = !showAlert;
    }
  }
  eventHandler(event) {
    if (event.keyCode === 13) {
      this.showNotification = !this.showNotification;
    }
  }
  toggle() {
    const btnState = this.renderer.selectRootElement('.slider-btn', true);
    if (btnState.checked) {
      btnState.checked = false;
    } else {
      btnState.checked = true;
    }
    this.clickBinge();

  }
  clickBinge() {
    const btnState = this.renderer.selectRootElement('.slider-btn', true);
    if (!btnState.checked) {
      this.onState = false;
    } else {
      this.onState = true;
    }
  }
}
