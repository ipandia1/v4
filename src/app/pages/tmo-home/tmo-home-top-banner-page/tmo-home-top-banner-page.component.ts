import { Component, OnInit, HostListener, Renderer2 } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-tmo-home-top-banner-page',
  templateUrl: './tmo-home-top-banner-page.component.html',
  styleUrls: ['./tmo-home-top-banner-page.component.scss']
})
export class TmoHomeTopBannerPageComponent implements OnInit {

  // homePageJsonPath = 'assets/json/tmo-home/home.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-no-notification.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-autopay.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-nonPah-upgrade.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-nonPah-noupgrade.json';
  homePageJsonPath = 'assets/json/tmo-home/home-no-payment.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-new-payment-notification.json';
  // homePageJsonPath = 'assets/json/tmo-home/home-alert.json';

  homePageData: any;
  showNotification: boolean;
  isDataLoaded = false;
  isEmptyObject = '{}';
  isPaymentDue = true;
  isPaymentDueCount: number;
  dropdownidval = 'test';
  // notification
  showAlert = true;
  tooltipText = 'Tooltip text lorem ipsum dolor sit.';

  smallDevice = false;
  imageAlignment: any;
  ctaAlignment: any;
  selectDataPath = 'assets/json/mapping/data1.json'; // Button CTA  is right aligned
   totalItems: any;

  constructor(private commonService: CommonService, private renderer: Renderer2) {

    this.commonService.getJsonData(this.selectDataPath)
    .subscribe(responseJson => {
      this.totalItems = responseJson;
    });
  }
  @HostListener('click', ['$event']) onClick(e) {
    if (!e.target.classList.contains('notification-down')) {
      if (this.showNotification) {
        this.showNotification = false;
      }
    }
  }
  @HostListener('keyup', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    const sourceElement = event.srcElement;
    const notificationsSection = this.renderer.selectRootElement('#notifications-section', true);
    const checkElement = notificationsSection.contains(sourceElement);
    if (!checkElement) {
      this.showNotification = false;
    }
  }
  ngOnInit() {
    this.commonService.visibleHeader = false;
    this.commonService.getJsonData(this.homePageJsonPath)
      .subscribe(responseJson => {
        this.homePageData = responseJson;
        this.isDataLoaded = true;
        this.isPaymentDueCount = this.calculatingPaymentDueday();
      });
    this.commonService.showGlobalHeader();
    this.commonService.showGlobalFooter();

     this.imageAlignment = 'left'; // replace with  right/left
     // this.ctaAlignment  = 'center'; // replace with right/left/center

  }
  // calculating payment due date from current date
  calculatingPaymentDueday() {
    const d1 = new Date();
    const date1 = (d1.getMonth() + 1) + '/' + d1.getDate() + '/' + d1.getFullYear();
    const d2 = new Date(this.homePageData['paymentDetails'][0].paymentDueDate);
    const date2 = (d2.getMonth() + 1) + '/' + d2.getDate() + '/' + d2.getFullYear();
    function parseDate(str) {
      const mdy = str.split('/');
      return new Date(mdy[2], mdy[0] - 1, mdy[1]);
    }
    function datediff(first, second) {
      return Math.round((first - second) / (1000 * 60 * 60 * 24));
    }
    const count = datediff(parseDate(date1), parseDate(date2));
    if (count >= 4 && count <= 20) {
      this.isPaymentDue = true;
      return count;
    }
  }
  openNotification() {
    this.showNotification = true;
  }
  closeNotification() {
    this.showNotification = false;
  }
  closeAlert(showAlert) {
    this.showAlert = showAlert;
    if (showAlert.keyCode === 13) {
      this.showAlert = !showAlert;
    }
  }
  eventHandler(event) {
    if (event.keyCode === 13) {
      this.showNotification = !this.showNotification;
    }

  }
}
