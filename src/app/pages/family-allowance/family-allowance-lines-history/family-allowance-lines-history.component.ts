import { Component, OnInit, EventEmitter } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-family-allowance-lines-history',
  templateUrl: './family-allowance-lines-history.component.html',
  styleUrls: ['./family-allowance-lines-history.component.scss']
})
export class FamilyAllowanceLinesHistoryComponent implements OnInit {
  isDataLoaded = false;
  dashboardJsonPath = 'assets/json/family-allowance/dashboard.json';
  // dashboardJsonPath = 'assets/json/family-allowance/dashboard-restricted.json';
  //  dashboardJsonPath = 'assets/json/family-allowance/dashboard-restricted-no-edit.json';
  dashboardData = [];
  selectedLineData: any;
  selectedVal = 'select';
  lineHistoryData = [
    // tslint:disable-next-line:max-line-length
    { title: '8/14/18 10:42AM', data: 'Whenever minutes changed from 500 to No Allowance' },
    // tslint:disable-next-line:max-line-length
    { title: '0/00/18 00:00AM', data: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.' },
    // tslint:disable-next-line:max-line-length
    { title: '0/00/00 00:00AM', data: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.' },
    // tslint:disable-next-line:max-line-length
    { title: '0/00/00 00:00AM', data: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.' },
    // tslint:disable-next-line:max-line-length
    { title: '0/00/00 00:00AM', data: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.' },
    // tslint:disable-next-line:max-line-length
    { title: '0/00/00 00:00AM', data: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.' }
  ];
  name = 'Natasha';
  dynamicplaceholder = 'Find a line';
  dropdownText = 'Billing cycle';
  callBackValue = new EventEmitter<string>();
  selectedRange = 'SEP 27TH - OCT 26TH';
  selectline = 'selectline';
  datePeriod: Array<any> = [
    { name: 'JAN 27TH - FEB 26TH', disabled: true },
    { name: 'FEB 27TH - MAR 26TH', disabled: true },
    { name: 'MAR 27TH - APR 26TH', disabled: true },
    { name: 'APR 27TH - MAY 26TH', disabled: true },
    { name: 'MAY 27TH - JUN 26TH', disabled: true },
    { name: 'JUN 27TH - JUL 26TH', disabled: true },
    { name: 'JUL 27TH - AUG 26TH', disabled: true },
    { name: 'AUG 27TH - SEP 26TH', disabled: false },
    { name: 'SEP 27TH - OCT 26TH', disabled: false },
    { name: 'OCT 27TH - NOV 26TH', disabled: false },
    { name: 'NOV 27TH - DEC 26TH', disabled: false },
  ];
  constructor(private commonService: CommonService) {
    this.selectedLineData = this.lineHistoryData;
    commonService.showGlobalHeader();
  }
  ngOnInit() {
    this.commonService.getJsonData(this.dashboardJsonPath)
      .subscribe(responseJson => {
        this.dashboardData = responseJson;
        this.isDataLoaded = true;
      });
  }
  selectDatePeriod(value: string): void {
    this.selectedRange = value;
  }
  selectedSearchValue(event) {
    if (event === '') {
      this.selectedLineData = this.lineHistoryData;
    } else {
      this.selectedLineData = this.lineHistoryData.filter(data =>
        data.title === event);
    }
  }
  selectedValue(value: string): any {
    this.selectedVal = value;
    this.callBackValue.emit(this.selectedVal);
  }
}
