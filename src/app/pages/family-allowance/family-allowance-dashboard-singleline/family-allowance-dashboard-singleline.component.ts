import { Component, OnInit, ElementRef, ChangeDetectionStrategy,
         ChangeDetectorRef , EventEmitter, HostListener, Renderer2 } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import { CommonService } from '../../../services/common.service';
import { Button } from 'protractor';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-family-allowance-dashboard-singleline',
  templateUrl: './family-allowance-dashboard-singleline.component.html',
  styleUrls: ['./family-allowance-dashboard-singleline.component.scss']
})
export class FamilyAllowanceDashboardSingleLineComponent implements OnInit {
  dashboardJsonPath = 'assets/json/family-allowance/dashboard-1.json';
  // dashboardJsonPath = 'assets/json/family-allowance/dashboard-restricted.json';
  // dashboardJsonPath = 'assets/json/family-allowance/dashboard-restricted-no-edit.json';
  dashboardData: Array<Object>;
  isDataLoaded = false;
  selectedVal = 'Select';
  placeholder = 'Find a line';
  inputTextAlwaysAllow = '';
  inputTextNeverAllow = '';
  dynamicplaceholder = 'Find a line';
  historyChanges = 0;
  selectHistory = 'SEP 27TH - OCT 26TH';
  onState = true;
  visible: boolean;
  showHistory1 = false;
  disableButton = true;
  selectdashboard = 'selectmain';
  ddlselecthistory = 'ddlselecthis';
  accordianCount: number;
  activeAccordian = 0;
  // tslint:disable-next-line:max-line-length
  neverAllowedTooltip = 'Block numbers from calling or messaging lines on your account. However, even if a number is on your Never Allowed list, it may still be reachable if you’re roaming on any domestic or international networks (and not on the T-Mobile USA network). International numbers, 800 numbers, and 900 numbers are not eligible for the Never Allowed list.';
  alwaysAllowedTooltip = 'Accept calls and message from specific numbers regardless of restrictions or allowance limits.';
  // tslint:disable-next-line:max-line-length
  ScheduleTooltop = 'These time periods were selected to best suit customer needs. If you need to block a 24-hour period, you should disable the phone on the individual line detail page. ';
  // tslint:disable-next-line:max-line-length
  parentLineTooltop = 'The Family Allowances Parent line sets limits for the other lines on the account. Please select a line below to serve as Parent line for Family Allowances. ';
  // tslint:disable-next-line:max-line-length
  noAllowancesTooltips = ['Setting No Allowance will allow this line to talk without restriction. This could result in overage charges on your bill. You can also restrict calling on this line by using the Schedule and Never Allow page.',
                          // tslint:disable-next-line:max-line-length
                          'Setting No Allowance will allow this line to text message without restriction. This could result in overage charges on your bill.',
                          // tslint:disable-next-line:max-line-length
                          'Setting “no allowance” will allow this line to continue talking with out restrictions. This could result in overage charges on your bill. You can also restrict calling on this line by using the Schedule and Never Allowed options.'];
  // tslint:disable-next-line:max-line-length
  allowancesAlertTooltips = ['Your usage may have reached or exceeded your set allowance due to calls to or from Always Allowed numbers, calls made while roaming, or changes made to your allowances.',
                            // tslint:disable-next-line:max-line-length
                            'Your usage may have reached or exceeded your set allowance due to messages sent to or from Always Allowed numbers, messages while roaming, or changes made to your allowances.',
                            // tslint:disable-next-line:max-line-length
                            'Setting No Allowance will allow this line to download without restriction. This could result in overage charges on your bill. You can also restrict calling on this line by using the Schedule and Never Allowed options.'];
  modalArray = {title: 'Are you sure you want to disable the phone? ',
  // tslint:disable-next-line:max-line-length
  content: 'When you disable the phone for one of your lines, you are restricting its usage to Always Allowed numbers only. All other allowances and restrictions you have set (Whenever Minutes®, Messages, Downloads, Included Minutes and Schedule restrictions) will not apply until you enable the phone. The phone will remain disabled until you change the status - it will not automatically be enabled at the beginning of your billing cycle.',
  primaryButton: 'Yes', secondaryButton: 'No'};
  selectedLineData = [];
  lineData = [
    { title: 'Natasha', id: '(000) 000-0000', mobile: '(000) 000-0000' },
    { title: 'Justin', id: '(000) 000-0000', mobile: '(000) 000-0000' },
    { title: 'Samantha', id: '(000) 000-0000', mobile: '(000) 000-0000' },
    { title: '(000) 000-0000', id: '(000) 000-0000', mobile: '(000) 000-0000' }
  ];
  dropdownText = 'This is the person authorized to create and modify allowances';
  callBackValue = new EventEmitter<string>();
  public currentWindowWidth: number;
  constructor(private commonService: CommonService,private elementref: ElementRef,
     private refresh: ChangeDetectorRef, private renderer: Renderer2) {
    this.selectedLineData = this.lineData;
    this.currentWindowWidth = window.innerWidth;
    commonService.showGlobalHeader();
  }

  ngOnInit() {
    this.commonService.getJsonData(this.dashboardJsonPath)
      .subscribe(responseJson => {
        this.dashboardData = responseJson;
        this.isDataLoaded = true;
        this.refresh.detectChanges();
        for (let i = 1; i < this.elementref.nativeElement.querySelectorAll('.acc-content').length; i++) {
          this.elementref.nativeElement.querySelectorAll('.acc-content')[i].classList.add('accordian-out');
          this.elementref.nativeElement.querySelectorAll('.arrow-ref')[i].classList.add('arrow-down-disable', 'arrow-down-rotation');
        }
      });
  }

  toggleAccordian(event, index) {
    const keycode = event.keyCode;
    if (keycode === 13) {
      this.closeAccordian(index);
    }
  }
  closeAccordian(j) {
    if (this.activeAccordian === j) {
      this.elementref.nativeElement.querySelectorAll('.acc-content')[j].classList.remove('accordian-in');
      this.elementref.nativeElement.querySelectorAll('.acc-content')[j].classList.add('accordian-out');
      this.elementref.nativeElement.querySelectorAll('.arrow-ref')[j].classList.remove('arrow-up', 'arrow-up-rotation');
      this.elementref.nativeElement.querySelectorAll('.arrow-ref')[j].classList.add('arrow-down-disable', 'arrow-down-rotation');
      this.activeAccordian = - 1 ;
    } else {
      this.activeAccordian = j;
      for (let i = 0; i < this.elementref.nativeElement.querySelectorAll('.acc-content').length; i++) {
        this.elementref.nativeElement.querySelectorAll('.acc-content')[i].classList.remove('accordian-in');
        this.elementref.nativeElement.querySelectorAll('.acc-content')[i].classList.add('accordian-out');
        this.elementref.nativeElement.querySelectorAll('.arrow-ref')[i].classList.remove('arrow-up', 'arrow-up-rotation');
        this.elementref.nativeElement.querySelectorAll('.arrow-ref')[i].classList.add('arrow-down-disable', 'arrow-down-rotation');
      }
      this.elementref.nativeElement.querySelectorAll('.acc-content')[j].classList.remove('accordian-out');
      this.elementref.nativeElement.querySelectorAll('.acc-content')[j].classList.add('accordian-in');
      this.elementref.nativeElement.querySelectorAll('.arrow-ref')[j].classList.remove('arrow-down-disable', 'arrow-down-rotation');
      this.elementref.nativeElement.querySelectorAll('.arrow-ref')[j].classList.add('arrow-up', 'arrow-up-rotation');
    }
  }

  selectedValue(value: string): any {
    this.selectedVal = value;
    this.callBackValue.emit(this.selectedVal);
 //   localStorage.setItem('selectedval', value);
  }
  select(value: string): void {
    this.selectedVal = value;
  }
  selecthistory(value: string, index): void {
    this.selectHistory = value;
    this.historyChanges = index;
  }
  addAllowable(inputTextAlwaysAllow) {
    if (inputTextAlwaysAllow) {
      const obj = { 'number': inputTextAlwaysAllow, 'description': '', 'removable': true };
      this.dashboardData['allowableNumbers'].push(obj);
      this.inputTextAlwaysAllow = '';
    }
  }
  addNeverAllowable(inputTextNeverAllow) {
    if (inputTextNeverAllow) {
      const obj = { 'number': inputTextNeverAllow, 'description': '', 'removable': true };
      this.dashboardData['neverAllowableNumbers'].push(obj);
      this.inputTextNeverAllow = '';
    }
  }
  removeAllowable(removingNumber) {
    this.dashboardData['allowableNumbers'].forEach(function (obj, index, object) {
      if (obj.number === removingNumber) {
        object.splice(index, 1);
      }
    });
  }
  removeNeverAllowable(removingNumber) {
    this.dashboardData['neverAllowableNumbers'].forEach(function (obj, index, object) {
      if (obj.number === removingNumber) {
        object.splice(index, 1);
      }
    });
  }

  toggle() {
    const btnState = this.renderer.selectRootElement('.slider-btn', true);
    console.log(btnState);
    if (btnState.checked) {
    btnState.checked =  false;
  } else {
      btnState.checked =  true;
    }

    this.clickFun();
  }
  clickFun() {

    const btnState = this.renderer.selectRootElement('.slider-btn', true);
    console.log(btnState.checked);
    if (!btnState.checked) {
      this.visible = !this.visible;
      const body = this.renderer.selectRootElement('body', true);
      // body.classList.add('noScroll');
      this.renderer.addClass(body, 'noScroll');
      const dialog = this.renderer.selectRootElement('#dialogs', true);
      dialog.focus();
      const buttonNo = this.renderer.selectRootElement('#buttonno', true);
        buttonNo.addEventListener('keydown', function (e) {
    e.preventDefault();
      const key = e.keyCode;
      if (key === 9) {
        const dialogFocus = this.renderer.selectRootElement('#dialogs', true);
        dialogFocus.focus();
      }
  });
    } else {
      btnState.checked = true;
      this.onState = true;
    }
  }
  primaryClose() {
    const btnState = this.renderer.selectRootElement('.slider-btn', true);
    btnState.checked = false;
    this.visible = false;
    this.onState = false;
    const body = this.renderer.selectRootElement('body', true);
    body.classList.remove('noScroll');
  }
  secondaryClose() {
    const btnState = this.renderer.selectRootElement('.slider-btn', true);
    btnState.checked = true;
    this.visible = false;
    this.onState = true;
    const body = this.renderer.selectRootElement('body', true);
    body.classList.remove('noScroll');
  }
  searchValue(event) {
    if (event === '') {
      this.selectedLineData = this.lineData;
    } else {
      this.selectedLineData = this.lineData.filter(data =>
        data.title === event);
    }
  }
  onResize(event) {
    this.currentWindowWidth = event.target.innerWidth;
  }
}
