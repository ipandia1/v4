import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-family-controls',
  templateUrl: './family-controls.component.html',
  styleUrls: ['./family-controls.component.scss']
})
export class FamilyControlsComponent implements OnInit {
  data: Array<any> = [
    {
      titleText: 'Web guard',
      // tslint:disable-next-line:max-line-length
      titleDesc: 'Prevent access to adult web content. <br>Does not restrict content when browsing over Wi-Fi, when visiting secured (HTTPS) websites, or accessing content via any application.',
      titleDesc2: 'Web content filter: No Restrictions', url: ''
    },
    {
      titleText: 'Family allowances', titleDesc: 'Set allowances on minutes, messages and downloads',
      titleDesc2: '', url: '../../family-allowance/fa-dashboard'
    },
    { titleText: 'Family where', titleDesc: 'Locate your family members device\'s on a map', titleDesc2: '', url: '' }
  ];
  dynamicplaceholder = 'Find a line';
  selectedLineData = [];
  lineData = [
    { title: 'Natasha', id: '(000) 000-0000', mobile: '(000) 000-0000' },
    { title: 'Justin', id: '(000) 000-0000', mobile: '(000) 000-0000' },
    { title: 'Samantha', id: '(000) 000-0000', mobile: '(000) 000-0000' }
  ];

  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.selectedLineData = this.lineData;
  }

  searchValue(event) {
    if (event === '') {
      this.selectedLineData = this.lineData;
    } else {
      this.selectedLineData = this.lineData.filter(data =>
        data.title === event);
    }
  }

}
