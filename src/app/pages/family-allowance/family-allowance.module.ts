import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FamilyAllowanceRoutingModule } from './family-allowance.routing';
import { FamilyControlsComponent } from './family-controls/family-controls.component';
import { FamilyAllowanceDashboardComponent } from './family-allowance-dashboard/family-allowance-dashboard.component';
import { FamilyAllowanceLinesComponent } from './family-allowance-lines/family-allowance-lines.component';
import { FamilyAllowanceLinesHistoryComponent } from './family-allowance-lines-history/family-allowance-lines-history.component';
import { BenefitComponent } from './benefit/benefit.component';
// tslint:disable-next-line:max-line-length
import { FamilyAllowanceDashboardSingleLineComponent } from './family-allowance-dashboard-singleline/family-allowance-dashboard-singleline.component';
import { ReusableModule } from '../../ui-components/reusable/reusable.module';

@NgModule({
  imports: [
    CommonModule, FamilyAllowanceRoutingModule, ReusableModule, FormsModule, ReactiveFormsModule
  ],
  declarations: [FamilyControlsComponent,
    FamilyAllowanceDashboardComponent, FamilyAllowanceLinesComponent, FamilyAllowanceLinesHistoryComponent,
    FamilyAllowanceDashboardSingleLineComponent, BenefitComponent]
})
export class FamilyAllowanceModule { }
