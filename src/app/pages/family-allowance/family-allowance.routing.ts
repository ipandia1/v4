import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FamilyControlsComponent } from './family-controls/family-controls.component';
import { FamilyAllowanceDashboardComponent } from './family-allowance-dashboard/family-allowance-dashboard.component';
import { FamilyAllowanceLinesComponent } from './family-allowance-lines/family-allowance-lines.component';
import { FamilyAllowanceLinesHistoryComponent } from './family-allowance-lines-history/family-allowance-lines-history.component';
import { FamilyAllowanceDashboardSingleLineComponent } from './family-allowance-dashboard-singleline/family-allowance-dashboard-singleline.component';
import { BenefitComponent } from './benefit/benefit.component';

const routes: Routes = [
  { path: 'fa-controls', component: FamilyControlsComponent },
  { path: 'fa-dashboard', component: FamilyAllowanceDashboardComponent },
  { path: 'fa-lines', component: FamilyAllowanceLinesComponent },
  { path: 'fa-lines-history', component: FamilyAllowanceLinesHistoryComponent },
  { path: 'fa-dashboard-singleline', component: FamilyAllowanceDashboardSingleLineComponent },
  { path: 'fa-benefit', component: BenefitComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FamilyAllowanceRoutingModule { }
