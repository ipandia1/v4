import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { HeaderComponent } from '../../../ui-components/reusable/header/header.component';

@Component({
  selector: 'app-family-allowance-lines',
  templateUrl: './family-allowance-lines.component.html',
  styleUrls: ['./family-allowance-lines.component.scss']
})
export class FamilyAllowanceLinesComponent implements OnInit {
  selectedLineData = [];
  lineData = [
    { title: 'Natasha' }, { title: '(000) 000-0000' }, { title: '(000) 000-0000' },
    { title: 'Justin' }, { title: '(000) 000-0000' }, { title: '(000) 000-0000' },
    { title: 'Samantha' }, { title: '(000) 000-0000' }, { title: '(000) 000-0000' },
    { title: '(000) 000-0000' }, { title: '(000) 000-0000' }, { title: '(000) 000-0000' }
  ];

  dynamicplaceholder = 'Find a line';
  constructor(private commonService: CommonService) {
    this.selectedLineData = this.lineData;
  }

  ngOnInit() {
    // this.commonService.hideHeader();
  }
  searchValue(event) {
    if (event === '') {
      this.selectedLineData = this.lineData;
    } else {
      this.selectedLineData = this.lineData.filter
        (item => item.title.toLowerCase().indexOf(event.toLowerCase()) !== -1);
    }
  }
}
